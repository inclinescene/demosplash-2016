//music.frag
// by cxw/incline.  CC-BY-SA 3.0
// Note frequencies: http://www.phy.mtu.edu/~suits/notefreqs.html
// Working example from iq: https://www.shadertoy.com/view/ldXXDj

// CONFIG /////////////////////////////////////////////

// play it safe.
precision highp int;
precision highp float;
    // per https://www.khronos.org/registry/gles/specs/2.0/GLSL_ES_Specification_1.0.17.pdf
    // sec. 4.5.3, these precisions also cover vectors and matrices.

#define PI (3.1415926535897932384626433832795028841971)
    // from memory :)
# define TWO_PI (6.283185307179586)
#define ONE_OVER_TWO_PI (0.15915494309644431437107064141535)
    // not from memory :) :)

#define ONE_THIRD (0.3333333333333333333333333333333)

float do_noise(in float time);
float do_kick(in float beat_in_pattern, in float time_in_beat);
    // constant each pattern throughout
float do_snap(in float time, in float beat_in_pattern, in float time_in_beat);
float do_clap(in float time, in float beat_in_pattern, in float time_in_beat);

// Song parameters
#define BPM (120.0)
    // beats per min
#define BPS (2.0)
    // beats per sec
#define SPB (0.5)
    // sec per beat

#define BPP (16.0)
    // beats per pattern
#define BPB (4.0)
    // beats per bar

// MAIN ///////////////////////////////////////////////

vec2 mainSound( float time )
{
    if(time<1.0) return vec2(0.0);
    float tshift = time - 1.0;
        // There seem to be some transients at startup - squelch them.

    float beat_in_pattern = mod(tshift*BPS, BPP);
        // including fraction
    float time_in_beat = mod(tshift, SPB);

    //Instruments
    float noise = do_noise(tshift);
    float kick = do_kick(beat_in_pattern, time_in_beat);
    float snap = do_snap(tshift, beat_in_pattern, time_in_beat);
    float clap = do_clap(tshift, beat_in_pattern, time_in_beat);

    //Mixer
    //return vec2(dot(vec3(kick, snap, clap),vec3(ONE_THIRD)));
    return vec2((kick+snap+clap)*ONE_THIRD);
    // --- EXIT POINT ---

    // Chord
    float TwoPiT = tshift*TWO_PI;
    vec3 amplitudes = vec3(ONE_THIRD);
    vec3 chord_notes =  //C4            E4                 G4
        vec3(sin(TwoPiT*261.63), sin(TwoPiT*329.63), sin(TwoPiT*392.00));
    float chord = dot(amplitudes, chord_notes);
    float pan = (sin(time*0.5)+1.0)*0.5;      //0=L to 1=R
    //return vec2(chord);
    return vec2(mix(0.0, chord, 1.0-pan), mix(0.0, chord, pan));
        // After an initial startup, it pans OK, but the notes of the chord
        // come in one at a time.  Very strange.  I guess don't hold
        // sustained notes :)
} //mainSound

#if 0
// Sample from shadertoy
vec2 mainSound( float time )
{
    return vec2( sin(6.2831*440.0*time)*exp(-3.0*time) );
}
#endif

// UTIL ///////////////////////////////////////////////
float scale01(in float f)
{   //rescale [0,1] to [-1,1]
    return mix(-1.0, 1.0, clamp(f, 0.0, 1.0));
}

// NOISE //////////////////////////////////////////////

#if 0
// From https://github.com/ashima/webgl-noise/blob/master/src/classicnoise2D.glsl
// GLSL textureless classic 2D noise "cnoise",
// with an RSL-style periodic variant "pnoise".
// Author:  Stefan Gustavson (stefan.gustavson@liu.se)
// Version: 2011-08-22
//
// Many thanks to Ian McEwan of Ashima Arts for the
// ideas for permutation and gradient selection.
//
// Copyright (c) 2011 Stefan Gustavson. All rights reserved.
// Distributed under the MIT license. See LICENSE file.
// https://github.com/stegu/webgl-noise
//

vec4 mod289(vec4 x)
{
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec4 permute(vec4 x)
{
  return mod289(((x*34.0)+1.0)*x);
}

vec4 taylorInvSqrt(vec4 r)
{
  return 1.79284291400159 - 0.85373472095314 * r;
}

vec2 fade(vec2 t) {
  return t*t*t*(t*(t*6.0-15.0)+10.0);
}

// Classic Perlin noise
float cnoise(vec2 P)
{
  vec4 Pi = floor(P.xyxy) + vec4(0.0, 0.0, 1.0, 1.0);
  vec4 Pf = fract(P.xyxy) - vec4(0.0, 0.0, 1.0, 1.0);
  Pi = mod289(Pi); // To avoid truncation effects in permutation
  vec4 ix = Pi.xzxz;
  vec4 iy = Pi.yyww;
  vec4 fx = Pf.xzxz;
  vec4 fy = Pf.yyww;

  vec4 i = permute(permute(ix) + iy);

  vec4 gx = fract(i * (1.0 / 41.0)) * 2.0 - 1.0 ;
  vec4 gy = abs(gx) - 0.5 ;
  vec4 tx = floor(gx + 0.5);
  gx = gx - tx;

  vec2 g00 = vec2(gx.x,gy.x);
  vec2 g10 = vec2(gx.y,gy.y);
  vec2 g01 = vec2(gx.z,gy.z);
  vec2 g11 = vec2(gx.w,gy.w);

  vec4 norm = taylorInvSqrt(vec4(dot(g00, g00), dot(g01, g01), dot(g10, g10), dot(g11, g11)));
  g00 *= norm.x;  
  g01 *= norm.y;  
  g10 *= norm.z;  
  g11 *= norm.w;  

  float n00 = dot(g00, vec2(fx.x, fy.x));
  float n10 = dot(g10, vec2(fx.y, fy.y));
  float n01 = dot(g01, vec2(fx.z, fy.z));
  float n11 = dot(g11, vec2(fx.w, fy.w));

  vec2 fade_xy = fade(Pf.xy);
  vec2 n_x = mix(vec2(n00, n01), vec2(n10, n11), fade_xy.x);
  float n_xy = mix(n_x.x, n_x.y, fade_xy.y);
  return 2.3 * n_xy;
}

// Classic Perlin noise, periodic variant
float pnoise(vec2 P, vec2 rep)
{
  vec4 Pi = floor(P.xyxy) + vec4(0.0, 0.0, 1.0, 1.0);
  vec4 Pf = fract(P.xyxy) - vec4(0.0, 0.0, 1.0, 1.0);
  Pi = mod(Pi, rep.xyxy); // To create noise with explicit period
  Pi = mod289(Pi);        // To avoid truncation effects in permutation
  vec4 ix = Pi.xzxz;
  vec4 iy = Pi.yyww;
  vec4 fx = Pf.xzxz;
  vec4 fy = Pf.yyww;

  vec4 i = permute(permute(ix) + iy);

  vec4 gx = fract(i * (1.0 / 41.0)) * 2.0 - 1.0 ;
  vec4 gy = abs(gx) - 0.5 ;
  vec4 tx = floor(gx + 0.5);
  gx = gx - tx;

  vec2 g00 = vec2(gx.x,gy.x);
  vec2 g10 = vec2(gx.y,gy.y);
  vec2 g01 = vec2(gx.z,gy.z);
  vec2 g11 = vec2(gx.w,gy.w);

  vec4 norm = taylorInvSqrt(vec4(dot(g00, g00), dot(g01, g01), dot(g10, g10), dot(g11, g11)));
  g00 *= norm.x;  
  g01 *= norm.y;  
  g10 *= norm.z;  
  g11 *= norm.w;  

  float n00 = dot(g00, vec2(fx.x, fy.x));
  float n10 = dot(g10, vec2(fx.y, fy.y));
  float n01 = dot(g01, vec2(fx.z, fy.z));
  float n11 = dot(g11, vec2(fx.w, fy.w));

  vec2 fade_xy = fade(Pf.xy);
  vec2 n_x = mix(vec2(n00, n01), vec2(n10, n11), fade_xy.x);
  float n_xy = mix(n_x.x, n_x.y, fade_xy.y);
  return 2.3 * n_xy;
}
#endif

float rand(vec2 co){
    // From http://stackoverflow.com/a/4275343
    // by http://stackoverflow.com/users/350875/appas
    // Returns 0..1
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}
// MUSIC //////////////////////////////////////////////

// Instruments
float do_noise(in float time)
{
    if(time<0.0) return 0.0;
    float pixnum = time*iSampleRate;
    vec2 texcoord;
    texcoord.s = mod(pixnum, 256.0)/256.0;
    texcoord.t = mod(floor(pixnum/256.0),256.0)/256.0;
    return scale01(rand(texcoord)); //scale01(texture2D(iChannel0,texcoord).r);
} //do_noise

#define KICK_ATTACK (0.008)  //ms
#define KICK_DSR (0.1)     //delay, sustain, release
#define KSS (0.8000)    //Kick Syncopated Start - initially 0.9375 - tweaked

float do_kick(in float beat_in_pattern, in float time_in_beat)
{
    float local_time;   // note-on time

    // Downbeat (0)
    if(beat_in_pattern<0.5) local_time = time_in_beat;

    //16th before beat 1
    else if(beat_in_pattern<KSS) local_time=0.0; //0.0=>no sound
    else if(beat_in_pattern<1.0) local_time = time_in_beat-(KSS*SPB);
                // subtract the time in this beat that has already gone by.
    //beat 1.5
    else if(beat_in_pattern<1.5) local_time=0.0;
    else if(beat_in_pattern<2.0) local_time = time_in_beat-(0.5*SPB);


    // Beat 8
    else if(beat_in_pattern<8.0) local_time=0.0;
    else if(beat_in_pattern<8.5) local_time = time_in_beat;

    //16th before beat 9
    else if(beat_in_pattern<(8.0+KSS)) local_time=0.0; //0.0=>no sound
    else if(beat_in_pattern<9.0) local_time = time_in_beat-(KSS*SPB);
                // subtract the time in this beat that has already gone by.
    //beat 9.5
    else if(beat_in_pattern<9.5) local_time=0.0;
    else if(beat_in_pattern<10.0) local_time = time_in_beat-(0.5*SPB);

    //beat 10.5
    else if(beat_in_pattern<10.5) local_time=0.0;
    else if(beat_in_pattern<11.0) local_time = time_in_beat-(0.5*SPB);

    else local_time = 0.0;

    float kick_envelope = smoothstep(0.0,KICK_ATTACK,local_time) *
                          (1.0-smoothstep(KICK_ATTACK,KICK_ATTACK+KICK_DSR,
                                            local_time));
    float kick = sin(80.0*TWO_PI*local_time)*kick_envelope;
        //TODO chirp from 90 Hz down to 44 Hz over 100 ms - attack 30ms,
        // decay the rest
    return kick;
} //do_kick

// snap is 2205 Hz for 4 ms plus decaying noise for a total of 45 ms
#define SNAP_TONE_TIME (0.004)
#define SNAP_TONE_FREQ (2205.0)
#define SNAP_NOISE_END_TIME (0.045)

float do_snap(in float time, in float beat_in_pattern, in float time_in_beat)
{
    float beat_in_bar = mod(beat_in_pattern, BPB);
    float local_time;   // note-on time

    if(beat_in_bar<1.0) local_time = -1.0;
    else if(beat_in_bar<1.2) local_time = time_in_beat;
    else local_time = -1.0;

    if(local_time<0.0) return 0.0;

    float snap_tone_envelope = 
        1.0-smoothstep(0.0,SNAP_TONE_TIME,local_time);

    float tone = sin(SNAP_TONE_FREQ*TWO_PI*local_time)*snap_tone_envelope;

    //Noise
    float snap_noise_envelope = 
        step(SNAP_TONE_TIME, local_time) *
        (1.0-smoothstep(SNAP_TONE_TIME,SNAP_NOISE_END_TIME,local_time));

    float pixnum = time*iSampleRate;
    vec2 texcoord;
    texcoord.s = mod(pixnum, 256.0)/256.0;
    texcoord.t = mod(floor(pixnum/256.0),256.0)/256.0;

    float noise = snap_noise_envelope*do_noise(time);

    return 0.5*tone+0.5*noise;
}

// clap is about 60ms of noise with an initial decay of 8ms to about
// half-amplitude.
#define CLAP_TIME (0.060)
#define CLAP_INITIAL_DECAY (0.008)

float do_clap(in float time, in float beat_in_pattern, in float time_in_beat)
{
    float beat_in_bar = mod(beat_in_pattern, BPB);
    float local_time;   // note-on time

    //beat 4
    if(beat_in_bar<3.0) local_time = -1.0;
    else if(beat_in_bar<3.5) local_time = time_in_beat;
    else local_time = -1.0;

    if(local_time<0.0) return 0.0;

    float initial_decay = 
        (1.0-step(CLAP_INITIAL_DECAY, local_time)) *    //1->0 at 8ms
            mix(0.0, 0.5, clamp(local_time/CLAP_INITIAL_DECAY,0.0,1.0));
                // 0@0, 0.5@8ms on
    initial_decay = 1.0-initial_decay;  //1.0, dropping to 0.5@8ms, then
                                        //back to 1.0
    float noise_envelope = 
        1.0-smoothstep(0.0,CLAP_TIME,local_time);
    float noise = noise_envelope * initial_decay * do_noise(time);
    return noise;
} //do_clap

// TODO looks like at least one voice is a sin(f) + 0.2*sin(11*f)

// vi: set ts=4 sts=4 sw=4 et ai: //

