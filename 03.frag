precision highp int;precision highp float;

// YOU WANT THIS: pages 3 and 4 of 
// https://www.khronos.org/files/webgl/webgl-reference-card-1_0.pdf

vec4 do_color(in float time, in vec2 coords)
{
    float whereami = 
        50.0*distance(vec2(0.5),coords) - 10.0*time;
    //  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^   ^^^^^^^^^
    //          frequency terms           phase terms
    //
    //  ^^^^ how many rings (50/2pi)      ^^^^ how fast they move (2pi/peak)
    //
    //       ^^^^^^^^^^^^^^^^^^^^^^^^^^ radial pattern
    return vec4(0.0,0.0,
                0.5+0.5*sin(whereami),  // render in the blue channel
                1.0);
} //do_color

float do_window(in float time, in float x)
{
    float window_pos = abs(0.5*sin(time));
        // from 0 to 0.5 and back, over and over again
    return step(window_pos,  x);
    //          ^^^^^^^^^^ > ^  => 0.0 else 1.0
} //do_window

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    float t = iGlobalTime;
    vec2 uv = fragCoord.xy / iResolution.xy;
    vec4 scene_color = do_color(t, uv);
    float window =  1.0;    
        // or how about this?  (cxw: reset time first)
        // do_window(t, uv.x);
    fragColor = scene_color * window;
} //mainImage

// vi: set ts=4 sts=4 sw=4 et ai: //

