//08-msx.frag: Music
precision highp int;precision highp float;

// Parameters for your demo.  These should match 08-gfx!!

// Song parameters
#define BPS (1.8333333333333333333333333333333)//(1.5)
    // beats per sec = BPM/60 (110 bpm) (90 bpm)
#define SPB (0.54545454545454545454545454545455)//(0.66666666666666666666666666666667)
    // sec per beat, precomputed
#define BPP (128.0)
    // beats per pattern

#define MUSIC_START_TIME (1.0)
    // how long to wait before starting the music

// Computed parameters //////////////////////////////////////////////////
#define PATTERN_DURATION_SEC (SPB * BPP)
    // = SPP = S/B * B/P

// Constants ////////////////////////////////////////////////////////////
#define PI (3.1415926535897932384626433832795028841971)
    // from memory :)
#define TWO_PI (6.283185307179586)
#define ONE_OVER_TWO_PI (0.15915494309644431437107064141535)
    // not from memory :) :)
#define HALF_PI (1.5707963267948966192313216916398)
#define ONE_OVER_HALF_PI (0.63661977236758134307553505349006)

// Kick drum ////////////////////////////////////////////////////////////

#define KICK_ATTACK (0.008)  //ms
#define KICK_DSR (0.1)     //decay, sustain, release

float do_kick(in float beat_in_pattern, in float time_in_beat)
{
    float local_time;   // time since note-on

    float is_even_beat = step(1.0, mod(beat_in_pattern, 2.0));
    local_time = mix(0.0, time_in_beat, is_even_beat);
        // time stuck at 0 during odd beats (*B*); runs during beats 2 and 4
        // of each measure.

    float kick_envelope = smoothstep(0.0,KICK_ATTACK,local_time);   //attack
    kick_envelope *= (1.0-smoothstep(KICK_ATTACK,KICK_ATTACK+KICK_DSR, //DSR
                                            local_time));

    float kick_sample = sin(80.0*TWO_PI*local_time);    // The basic waveform
        // Using sin() => t=0 means no sound. (*B*)
    kick_sample *= kick_envelope;                       // Apply the envelope

    return kick_sample;
} //do_kick

// Melody (square?) /////////////////////////////////////////////////////

#define MELODY_RAMP_TIME_SEC (0.05)
    // Attack/release time

// BEGIN material from gen-music.py
// --- VVV cutbegin ---
// Tuning
#define F_REST (0.0)
    // for consistency
#define P_REST (0.0)
#define F_C4 (277.180)
#define P_C4 (0.00360776390792986491)
#define F_D4 (311.130)
#define P_D4 (0.00321409057307234930)
#define F_E4 (349.230)
#define P_E4 (0.00286344243048993483)
#define F_F4 (369.990)
#define P_F4 (0.00270277575069596473)
#define F_G4 (415.300)
#define P_G4 (0.00240789790512882258)
#define F_A4 (466.160)
#define P_A4 (0.00214518620216234751)
#define F_B4 (493.880)
#define P_B4 (0.00202478334818174477)
#define F_C5 (554.370)
#define P_C5 (0.00180384941465086502)
#define F_D5 (622.250)
#define P_D5 (0.00160707111289674562)
#define F_E5 (698.460)
#define P_E5 (0.00143172121524496741)
#define F_F5 (739.990)
#define P_F5 (0.00135136961310287971)
#define F_G5 (830.610)
#define P_G5 (0.00120393445780811697)
#define F_A5 (932.330)
#define P_A5 (0.00107258159664496474)
#define F_B5 (987.770)
#define P_B5 (0.00101238142482561731)
#define F_C6 (1108.730)
#define P_C6 (0.00090193284208057870)
#define F_D6 (1244.510)
#define P_D6 (0.00080352909980634948)

// Score
vec4 get_song_data(in float beat_in_pattern)
{
    //DuckTales Moon theme.
    //Music by Hiroshige Tonomura.  Transcribed by Manuel Gutierrez Rojas.
    //tweaked by cxw.
    vec4 retval; //(frequency, period (1/freq), startbeat, endbeat)
        // frequency < 1.0 => no sound
    retval = vec4(0.0);     // by default, no sound
    retval = mix(retval, vec4(F_REST, P_REST, 0.000, 2.000), step(0.000, beat_in_pattern));
    retval = mix(retval, vec4(F_A4, P_A4, 2.000, 2.500), step(2.000, beat_in_pattern));
    retval = mix(retval, vec4(F_B4, P_B4, 2.500, 3.000), step(2.500, beat_in_pattern));
    retval = mix(retval, vec4(F_C5, P_C5, 3.000, 4.000), step(3.000, beat_in_pattern));
    retval = mix(retval, vec4(F_C5, P_C5, 4.000, 5.000), step(4.000, beat_in_pattern));
    retval = mix(retval, vec4(F_F5, P_F5, 5.000, 6.000), step(5.000, beat_in_pattern));
    retval = mix(retval, vec4(F_E5, P_E5, 6.000, 7.000), step(6.000, beat_in_pattern));
    retval = mix(retval, vec4(F_F5, P_F5, 7.000, 8.000), step(7.000, beat_in_pattern));
    retval = mix(retval, vec4(F_G5, P_G5, 8.000, 9.000), step(8.000, beat_in_pattern));
    retval = mix(retval, vec4(F_A5, P_A5, 9.000, 9.500), step(9.000, beat_in_pattern));
    retval = mix(retval, vec4(F_F5, P_F5, 9.500, 15.000), step(9.500, beat_in_pattern));
    retval = mix(retval, vec4(F_E5, P_E5, 15.000, 15.500), step(15.000, beat_in_pattern));
    retval = mix(retval, vec4(F_F5, P_F5, 15.500, 16.000), step(15.500, beat_in_pattern));
    retval = mix(retval, vec4(F_C6, P_C6, 16.000, 17.500), step(16.000, beat_in_pattern));
    retval = mix(retval, vec4(F_F5, P_F5, 17.500, 18.000), step(17.500, beat_in_pattern));
    retval = mix(retval, vec4(F_F5, P_F5, 18.000, 23.000), step(18.000, beat_in_pattern));
    retval = mix(retval, vec4(F_C5, P_C5, 23.000, 23.500), step(23.000, beat_in_pattern));
    retval = mix(retval, vec4(F_F5, P_F5, 23.500, 24.000), step(23.500, beat_in_pattern));
    retval = mix(retval, vec4(F_F5, P_F5, 24.000, 26.000), step(24.000, beat_in_pattern));
    retval = mix(retval, vec4(F_REST, P_REST, 26.000, 26.500), step(26.000, beat_in_pattern));
    retval = mix(retval, vec4(F_G5, P_G5, 26.500, 27.000), step(26.500, beat_in_pattern));
    retval = mix(retval, vec4(F_F5, P_F5, 27.000, 27.500), step(27.000, beat_in_pattern));
    retval = mix(retval, vec4(F_E5, P_E5, 27.500, 32.000), step(27.500, beat_in_pattern));
    retval = mix(retval, vec4(F_A4, P_A4, 32.000, 34.000), step(32.000, beat_in_pattern));
    retval = mix(retval, vec4(F_REST, P_REST, 34.000, 34.500), step(34.000, beat_in_pattern));
    retval = mix(retval, vec4(F_G4, P_G4, 34.500, 35.000), step(34.500, beat_in_pattern));
    retval = mix(retval, vec4(F_F4, P_F4, 35.000, 35.500), step(35.000, beat_in_pattern));
    retval = mix(retval, vec4(F_C5, P_C5, 35.500, 37.000), step(35.500, beat_in_pattern));
    retval = mix(retval, vec4(F_REST, P_REST, 37.000, 38.000), step(37.000, beat_in_pattern));
    retval = mix(retval, vec4(F_F5, P_F5, 38.000, 39.000), step(38.000, beat_in_pattern));
    retval = mix(retval, vec4(F_G5, P_G5, 39.000, 40.000), step(39.000, beat_in_pattern));
    retval = mix(retval, vec4(F_B5, P_B5, 40.000, 41.500), step(40.000, beat_in_pattern));
    retval = mix(retval, vec4(F_A5, P_A5, 41.500, 42.000), step(41.500, beat_in_pattern));
    retval = mix(retval, vec4(F_A5, P_A5, 42.000, 46.000), step(42.000, beat_in_pattern));
    retval = mix(retval, vec4(F_C6, P_C6, 46.000, 47.000), step(46.000, beat_in_pattern));
    retval = mix(retval, vec4(F_B5, P_B5, 47.000, 48.000), step(47.000, beat_in_pattern));
    retval = mix(retval, vec4(F_A5, P_A5, 48.000, 48.500), step(48.000, beat_in_pattern));
    retval = mix(retval, vec4(F_B5, P_B5, 48.500, 49.000), step(48.500, beat_in_pattern));
    retval = mix(retval, vec4(F_A5, P_A5, 49.000, 49.500), step(49.000, beat_in_pattern));
    retval = mix(retval, vec4(F_F5, P_F5, 49.500, 50.500), step(49.500, beat_in_pattern));
    retval = mix(retval, vec4(F_C5, P_C5, 50.500, 52.500), step(50.500, beat_in_pattern));
    retval = mix(retval, vec4(F_G5, P_G5, 52.500, 53.000), step(52.500, beat_in_pattern));
    retval = mix(retval, vec4(F_F5, P_F5, 53.000, 53.500), step(53.000, beat_in_pattern));
    retval = mix(retval, vec4(F_E5, P_E5, 53.500, 54.000), step(53.500, beat_in_pattern));
    retval = mix(retval, vec4(F_D5, P_D5, 54.000, 54.500), step(54.000, beat_in_pattern));
    retval = mix(retval, vec4(F_E5, P_E5, 54.500, 55.000), step(54.500, beat_in_pattern));
    retval = mix(retval, vec4(F_C5, P_C5, 55.000, 55.500), step(55.000, beat_in_pattern));
    retval = mix(retval, vec4(F_REST, P_REST, 55.500, 56.000), step(55.500, beat_in_pattern));
    retval = mix(retval, vec4(F_F5, P_F5, 56.000, 57.300), step(56.000, beat_in_pattern));
    retval = mix(retval, vec4(F_F5, P_F5, 57.300, 58.600), step(57.300, beat_in_pattern));
    retval = mix(retval, vec4(F_F5, P_F5, 58.600, 59.600), step(58.600, beat_in_pattern));
    retval = mix(retval, vec4(F_REST, P_REST, 59.600, 59.600), step(59.600, beat_in_pattern));
    return retval;
}
// --- ^^^ cutend ---
// END   material from gen-music.py

float do_tri(in float time, in float freq_hz, in float period_sec)
{ // additively synthesize something vaguely like an NES triangle
    float phase_rad = mod(time, period_sec) * TWO_PI * freq_hz;
    float phase_within_quadrant = mod(phase_rad, HALF_PI);
    float phase_at_quadrant_start = phase_rad - phase_within_quadrant;
    float ramp_start = sin(phase_at_quadrant_start);
    float ramp_end = sin(phase_at_quadrant_start + HALF_PI);

    float pct = phase_within_quadrant * ONE_OVER_HALF_PI;
    float retval = mix(ramp_start, ramp_end, pct);

    retval += 0.2*cos(TWO_PI * freq_hz * time);             // sag/rise
    //retval += 0.05*sin(32.0 * TWO_PI * freq_hz * time);   // steps
        // Sounds less harsh on my machine without this.

    retval *= 0.9;      //leave a bit of room
    retval += 0.006;    //DC balance (sort of)

    return retval;
} //do_tri

float do_melody(in float beat_in_pattern, in float time)
{ //time is the global time

    // Get the current note (which may be a rest)
    vec4 note = get_song_data(beat_in_pattern);

    // Unpack the note
    float freq_hz = note[0];
    if(freq_hz<1.0) return 0.0;     //early bail on rests

    float period_sec = note[1];
    float start_beat = note[2]; // should be <= beat_in_pattern
    float end_beat = note[3];   // note covers [start_beat, end_beat)
    float time_in_note = max(0.0, time - (start_beat * SPB));
        // Note: "max: no overloaded function found" means you used
        // integer 0 instead of float 0.0

    //Make the sound
    float wave = do_tri(time, freq_hz, period_sec);

    // Envelope - smooth transition at beginning and end of note.

    //attack
    float envelope = smoothstep(0.0, MELODY_RAMP_TIME_SEC, time_in_note);

    // release
    float end_time = end_beat * SPB;
    envelope *= (1.0 - smoothstep(end_time - MELODY_RAMP_TIME_SEC,
                                    end_time, time_in_note));

    return wave * envelope;
} //do_melody

// mainSound() //////////////////////////////////////////////////////////

vec2 mainSound( float time )
{
    float t = time;
    if(t < MUSIC_START_TIME) { return vec2(0.0); }
    t -= MUSIC_START_TIME;
        //because I had better luck not starting right at t=0.
    float music_t = mod(t, PATTERN_DURATION_SEC); //within one pattern

    float beat_in_pattern = mod(music_t*BPS, BPP);

    float time_in_beat = mod(music_t, SPB);
    float kick = do_kick(beat_in_pattern, time_in_beat);    //mono

    float melody = do_melody(beat_in_pattern, music_t);     //mono

    return vec2(kick, 0.5*melody);        //make it stereo.

} //mainSound

// vi: set ts=4 sts=4 sw=4 et ai: //

