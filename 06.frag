// REMINDER: when you compile, check the shader's tab for a red outline!
// There may be a compile error offscreen.  If you recompile but nothing
// changes, there's probably an error.  ShaderToy uses the last shader
// it successfully compiled.

precision highp int;precision highp float;

// Parameters for your demo

// blinds
#define BLIND_CYCLE_TIME (3.0)
    // Over this time (sec), the blinds will open and then close
#define BLIND_RAMP_TIME (1.0)
    // How long they take to open or close.  Keep it < BLIND_CYCLE_TIME/2.0
#define NBLINDS (7)
    // An integer!!!!!1!!
#define BLIND_EDGE_STEP (0.14285714285714285714285714285714)
    // 1/NBLINDS, precomputed to avoid division

// moire
#define THICKNESS (0.85)
    // larger means narrower
#define SPACING (200.0)
    //ditto
#define SPEED (0.6)
    // Higher is faster

// Computed parameters //////////////////////////////////////////////////
#define BLIND_HALFCYCLE_TIME (BLIND_CYCLE_TIME*0.5)
#define BLIND_END_RAMPDOWN_TIME (BLIND_HALFCYCLE_TIME + BLIND_RAMP_TIME)
    // blinds begin closing halfway through the cycle, and are fully closed
    // at BLIND_END_RAMPDOWN_TIME.
#define BLIND_RAMP_RATE (1.0/BLIND_RAMP_TIME)
    // Laziness!  For the release, precompute this since the shader
    // may have lower precision than your calculator.

// Transition: Vertical blinds //////////////////////////////////////////
// Evenly-spaced vertical blinds can be done with mod().  Unevenly, though...

float get_blind_mask(in float percent, in float x_01)
{ // x_01 is X coord, 0..1 (not pixel).
  // percent is how open the blinds are.  ** No time in this function! **
  // return value is 0.0 for a pixel that is covered, and 1.0 for a pixel
  // that is not covered.

    float edges[NBLINDS+1];   // Array.  Can also be outside any function.
        // Blind x goes from edges[x] to edges[x+1].
        // edges[0]=0.0; edges[NBLINDS+1]=1.0.

    // Init (each frame!).  Note: "for" loops are very fixed-form.
    // Also note that "break" and "continue" work in loops.

    for(int edge_idx = 0; edge_idx < NBLINDS+1; ++edge_idx) {
        float edge_idx_f = float(edge_idx);   //explicit typecast
        edges[edge_idx] = smoothstep(0.0, 1.0, edge_idx_f * BLIND_EDGE_STEP);
    } //for each edge

    // See if we are in a blind
    for(int blind_idx = 0; blind_idx < NBLINDS; ++blind_idx) {
        float left_edge = edges[blind_idx];
        float right_edge = edges[blind_idx+1];
        float center = mix(left_edge, right_edge, 0.5);
        float open_left = mix(center, left_edge, percent);
        float open_right = mix(center, right_edge, percent);
        if( (x_01 >= open_left) && (x_01 <= open_right) ) {
            return 1.0;     // This pixel is visible!
        }
    } //for each blind

    return 0.0;     // If we get here, the pixel is hidden by the blinds.

} //get_blind_mask

// Effect: moire ////////////////////////////////////////////////////////

float get_sine_01( in float time, in vec2 coords, in vec2 center, in float radius, in float speed) { float whereami = radius*distance(center,coords) - speed*time; return 0.5+0.5*sin(whereami); }
float squish(in float f) { return (f+1.0)*0.5; }
vec2 lisa(in float time, in float a, in float b, in float delta) { return vec2(squish(sin(a*time+delta)), squish (sin(b*time))); }

vec4 moire(in float time, in vec2 pixel_coord_01 )
{ //used to be mainImage() - now it's moire() and has a "time" parameter.
    vec2 ca = lisa(time*0.5*SPEED,        5.0,4.0,0.0); vec2 cb = lisa(time*0.835744*SPEED,   3.0,2.0,1.8); cb = mix(ca, cb, 0.6); float sa = get_sine_01(time, pixel_coord_01, ca, SPACING, 10.0); float sb = get_sine_01(time, pixel_coord_01, cb, SPACING, 10.0); sa=step(THICKNESS,sa); sb=step(THICKNESS,sb); return vec4(max(sa,sb),0.0,0.0,1.0);
} //moire

// mainImage() //////////////////////////////////////////////////////////

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    float t = iGlobalTime;
    vec2 pixel_coord_01 = fragCoord.xy / iResolution.xy;
    fragColor = moire(t, pixel_coord_01);       // The effect

    // Apply the mask

    // Make a clipped, repeating triangular profile
    float blind_time = mod(t, BLIND_CYCLE_TIME);        //the repeat
    float blind_rise = blind_time * BLIND_RAMP_RATE;    //ramp up
    float blind_fall = (BLIND_END_RAMPDOWN_TIME - blind_time) * BLIND_RAMP_RATE;
                                                        //ramp down
    float blind_percent = clamp(min(blind_rise, blind_fall), 0.0, 1.0);
                                                        //clip
    // Is this pixel visible through the blinds?
    float blind_mask = get_blind_mask(blind_percent, pixel_coord_01.x);

    //Black out masked pixels
    fragColor = mix(vec4(0.0), fragColor, blind_mask);
        // use fragColor = vec4(vec3(blind_percent),1.0); to see just one value
} //mainImage

// vi: set ts=4 sts=4 sw=4 et ai: //

