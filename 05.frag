precision highp int;precision highp float;

// Parameters for your demo

#define THICKNESS (0.85)
    // larger means narrower
#define SPACING (200.0)
    //ditto
#define SPEED (0.6)
    // Higher is faster

float           //not a color, but an ingredient
get_sine_01(    //Named for its output range
    in float time, in vec2 coords,      // parameters!
    in vec2 center, in float radius, in float speed)
{
    float whereami = radius*distance(center,coords) - speed*time;
    return 0.5+0.5*sin(whereami);
} //get_sine_01

float squish(in float f)
{ //-1..1->0..1
  return (f+1.0)*0.5;
}
vec2 lisa(in float time, in float a, in float b, in float delta)
{ // a Lissajous curve.  Formula from en.wikipedia.org.
    return vec2(squish(sin(a*time+delta)), squish (sin(b*time)));
} //lisa

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    float t = iGlobalTime;
    vec2 pixel_coord = fragCoord.xy / iResolution.xy;

    // Centers of sines a and b.  All numbers empirical.
    vec2 ca = lisa(t*0.5*SPEED,        5.0,4.0,0.0);
    vec2 cb = lisa(t*0.835744*SPEED,   3.0,2.0,1.8);

    cb = mix(ca, cb, 0.6);      //Move cb closer to ca.
        // I did this because the centers were spending too much time apart
        // and this was an easy way to mitigate that.

    // Sine values
    float sa = get_sine_01(t, pixel_coord, ca, SPACING, 10.0);
    float sb = get_sine_01(t, pixel_coord, cb, SPACING, 10.0);

    // Make them sharp rather than blurry
    sa=step(THICKNESS,sa);
    sb=step(THICKNESS,sb);

    // Combine the two sines
    float res=max(sa,sb);
        // or min(sa,sb)   sa-sb   mix(sa,sb,0.5)
    fragColor = vec4(res,0.0,0.0,1.0);
       // or vec4(sa,sb,0.0,1.0);

} //mainImage

// vi: set ts=4 sts=4 sw=4 et ai: //

