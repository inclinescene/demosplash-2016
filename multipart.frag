//multipart.frag by cxw/incline.  CC-BY-SA 3.0

// CONFIG /////////////////////////////////////////////
// {{{1
precision highp int;    //play it safe.  Also covers
precision highp float;  //vectors and matrices.

#define S_SINE_FREQ (0.02380952380952380952380952380952)
    // 1/42.  sine letter frequency in cycles per X unit.
#define S_SINE_GROUP_FREQ (0.03125)
    // 1/32.  sine group frequency.  If > S_SINE_FREQ, sine gradually
    // shifts right on screen.
#define CYL_RADIUS (15.0)
    // radius of the cylinder for CYL and later parts
#define TUNNEL_ACCEL (17.0)
    // Acceleration during TUNNEL, in x units per sec^2
#define CUBE_FADEIN_TIME (3.0)
    //fadein in seconds

#define TWOSIDED_RATE (0.3)
    // how fast you go back and forth

#define PI (3.1415926535897932384626433832795028841971)
    // from memory :)
# define TWO_PI (6.283185307179586)
#define ONE_OVER_TWO_PI (0.15915494309644431437107064141535)
    // not from memory :) :)
// }}}1

// MESSAGE ////////////////////////////////////////////
// Between the cutbegin and cutend lines, paste the output of make-message.py
// {{{1
// --- VVV cutbegin ---
// Parts and start times
#define NOP (0.0)
#define NOP_START (0.00000000000000000000)
#define S_PLAIN (1.0)
#define S_PLAIN_START (1.00000000000000000000)
#define STARWARS (2.0)
#define STARWARS_START (23.25000000000000000000)
#define S_ZOOM (3.0)
#define S_ZOOM_START (49.36945789249084270978)
#define S_ROTO (4.0)
#define S_ROTO_START (73.61945789249084270978)
#define BOLD (5.0)
#define BOLD_START (89.61945789249084270978)
#define CYL (6.0)
#define CYL_START (95.61945789249084270978)
#define TUNNEL (7.0)
#define TUNNEL_START (104.61945789249084270978)
#define S_SINE (8.0)
#define S_SINE_START (114.61945789249084270978)
#define TWOSIDED (9.0)
#define TWOSIDED_START (135.11945789249085692063)
#define S_PLASMA (10.0)
#define S_PLASMA_START (159.11945789249085692063)
#define CUBE (11.0)
#define CUBE_START (184.61945789249085692063)

vec4 get_story(in float time)
{   //returns vec4(partnum, charidx_frac, first_charidx, clip_charidx)
    // NOTE: charidx_frac restarts at 0 each part!
    // Character indices starting with clip_charidx should not be displayed.
    float partnum, charidx_frac, first_charidx, clip_charidx;
    if(time<1.00000000000000000000) {
        partnum=NOP;
        charidx_frac=(time-NOP_START)*1.00000000000000000000;
        first_charidx=0.0;
        clip_charidx=0.0;
    } else

    if(time<23.25000000000000000000) {
        partnum=S_PLAIN;
        charidx_frac=(time-S_PLAIN_START)*4.00000000000000000000;
        first_charidx=0.0;
        clip_charidx=80.0;
    } else

    if(time<49.36945789249084270978) {
        partnum=STARWARS;
        charidx_frac=(time-STARWARS_START)*2.71828000000000002956;
        first_charidx=89.0;
        clip_charidx=159.0;
    } else

    if(time<73.61945789249084270978) {
        partnum=S_ZOOM;
        charidx_frac=(time-S_ZOOM_START)*4.00000000000000000000;
        first_charidx=160.0;
        clip_charidx=249.0;
    } else

    if(time<89.61945789249084270978) {
        partnum=S_ROTO;
        charidx_frac=(time-S_ROTO_START)*4.00000000000000000000;
        first_charidx=257.0;
        clip_charidx=317.0;
    } else

    if(time<95.61945789249084270978) {
        partnum=BOLD;
        charidx_frac=(time-BOLD_START)*2.33333333333333348136;
        first_charidx=321.0;
        clip_charidx=325.0;
    } else

    if(time<104.61945789249084270978) {
        partnum=CYL;
        charidx_frac=(time-CYL_START)*4.00000000000000000000;
        first_charidx=335.0;
        clip_charidx=364.0;
    } else

    if(time<114.61945789249084270978) {
        partnum=TUNNEL;
        charidx_frac=(time-TUNNEL_START)*1.00000000000000000000;
        first_charidx=371.0;
        clip_charidx=371.0;
    } else

    if(time<135.11945789249085692063) {
        partnum=S_SINE;
        charidx_frac=(time-S_SINE_START)*4.00000000000000000000;
        first_charidx=371.0;
        clip_charidx=448.0;
    } else

    if(time<159.11945789249085692063) {
        partnum=TWOSIDED;
        charidx_frac=(time-TWOSIDED_START)*4.00000000000000000000;
        first_charidx=453.0;
        clip_charidx=545.0;
    } else

    if(time<184.61945789249085692063) {
        partnum=S_PLASMA;
        charidx_frac=(time-S_PLASMA_START)*4.00000000000000000000;
        first_charidx=549.0;
        clip_charidx=646.0;
    } else

    if(time<1184.61945789249080007721) {
        partnum=CUBE;
        charidx_frac=(time-CUBE_START)*0.00700000000000000015;
        first_charidx=651.0;
        clip_charidx=658.0;
    } else

    {
        partnum=0.0;
        charidx_frac=0.0;
        first_charidx=0.0;
        clip_charidx=0.0;
    }

    return vec4(partnum,charidx_frac,first_charidx,clip_charidx);
} //get_story

float get_seg_mask(float charidx)
{
    if(charidx>=658.0) return 0.0; //blank at the end
    if(charidx>=329.0){
        if(charidx>=493.0){
            if(charidx>=575.0){
                if(charidx>=616.0){
                    if(charidx>=637.0){
                        if(charidx>=647.0){
                            if(charidx>=652.0){
                                if(charidx>=655.0){
                                    if(charidx>=657.0) return 187.0;
                                    if(charidx>=656.0) return 11.0;
                                    if(charidx>=655.0) return 21.0;
                                }else{
                                    if(charidx>=654.0) return 18.0;
                                    if(charidx>=653.0) return 19.0;
                                    if(charidx>=652.0) return 11.0;
                                }
                            }else{
                                if(charidx>=649.0){
                                    if(charidx>=651.0) return 21.0;
                                    if(charidx>=649.0) return 0.0;
                                }else{
                                    if(charidx>=647.0) return 0.0;
                                }
                            }
                        }else{
                            if(charidx>=642.0){
                                if(charidx>=644.0){
                                    if(charidx>=646.0) return 0.0;
                                    if(charidx>=645.0) return 118.0;
                                    if(charidx>=644.0) return 11.0;
                                }else{
                                    if(charidx>=643.0) return 123.0;
                                    if(charidx>=642.0) return 3.0;
                                }
                            }else{
                                if(charidx>=639.0){
                                    if(charidx>=641.0) return 114.0;
                                    if(charidx>=640.0) return 59.0;
                                    if(charidx>=639.0) return 0.0;
                                }else{
                                    if(charidx>=638.0) return 178.0;
                                    if(charidx>=637.0) return 3.0;
                                }
                            }
                        }
                    }else{
                        if(charidx>=626.0){
                            if(charidx>=631.0){
                                if(charidx>=634.0){
                                    if(charidx>=636.0) return 27.0;
                                    if(charidx>=634.0) return 59.0;
                                }else{
                                    if(charidx>=633.0) return 26.0;
                                    if(charidx>=632.0) return 211.0;
                                    if(charidx>=631.0) return 0.0;
                                }
                            }else{
                                if(charidx>=628.0){
                                    if(charidx>=630.0) return 27.0;
                                    if(charidx>=629.0) return 3.0;
                                    if(charidx>=628.0) return 118.0;
                                }else{
                                    if(charidx>=627.0) return 123.0;
                                    if(charidx>=626.0) return 15.0;
                                }
                            }
                        }else{
                            if(charidx>=621.0){
                                if(charidx>=623.0){
                                    if(charidx>=625.0) return 0.0;
                                    if(charidx>=624.0) return 211.0;
                                    if(charidx>=623.0) return 178.0;
                                }else{
                                    if(charidx>=622.0) return 123.0;
                                    if(charidx>=621.0) return 187.0;
                                }
                            }else{
                                if(charidx>=618.0){
                                    if(charidx>=620.0) return 242.0;
                                    if(charidx>=619.0) return 0.0;
                                    if(charidx>=618.0) return 30.0;
                                }else{
                                    if(charidx>=617.0) return 124.0;
                                    if(charidx>=616.0) return 19.0;
                                }
                            }
                        }
                    }
                }else{
                    if(charidx>=595.0){
                        if(charidx>=605.0){
                            if(charidx>=610.0){
                                if(charidx>=613.0){
                                    if(charidx>=615.0) return 0.0;
                                    if(charidx>=614.0) return 187.0;
                                    if(charidx>=613.0) return 248.0;
                                }else{
                                    if(charidx>=612.0) return 27.0;
                                    if(charidx>=611.0) return 19.0;
                                    if(charidx>=610.0) return 0.0;
                                }
                            }else{
                                if(charidx>=607.0){
                                    if(charidx>=609.0) return 16.0;
                                    if(charidx>=607.0) return 235.0;
                                }else{
                                    if(charidx>=606.0) return 18.0;
                                    if(charidx>=605.0) return 19.0;
                                }
                            }
                        }else{
                            if(charidx>=600.0){
                                if(charidx>=602.0){
                                    if(charidx>=604.0) return 11.0;
                                    if(charidx>=602.0) return 0.0;
                                }else{
                                    if(charidx>=601.0) return 258.0;
                                    if(charidx>=600.0) return 243.0;
                                }
                            }else{
                                if(charidx>=597.0){
                                    if(charidx>=599.0) return 72.0;
                                    if(charidx>=598.0) return 235.0;
                                    if(charidx>=597.0) return 185.0;
                                }else{
                                    if(charidx>=596.0) return 0.0;
                                    if(charidx>=595.0) return 11.0;
                                }
                            }
                        }
                    }else{
                        if(charidx>=585.0){
                            if(charidx>=590.0){
                                if(charidx>=592.0){
                                    if(charidx>=594.0) return 21.0;
                                    if(charidx>=593.0) return 0.0;
                                    if(charidx>=592.0) return 211.0;
                                }else{
                                    if(charidx>=591.0) return 118.0;
                                    if(charidx>=590.0) return 3.0;
                                }
                            }else{
                                if(charidx>=587.0){
                                    if(charidx>=589.0) return 27.0;
                                    if(charidx>=588.0) return 30.0;
                                    if(charidx>=587.0) return 0.0;
                                }else{
                                    if(charidx>=585.0) return 18.0;
                                }
                            }
                        }else{
                            if(charidx>=580.0){
                                if(charidx>=582.0){
                                    if(charidx>=584.0) return 21.0;
                                    if(charidx>=583.0) return 178.0;
                                    if(charidx>=582.0) return 211.0;
                                }else{
                                    if(charidx>=581.0) return 0.0;
                                    if(charidx>=580.0) return 219.0;
                                }
                            }else{
                                if(charidx>=577.0){
                                    if(charidx>=579.0) return 11.0;
                                    if(charidx>=578.0) return 21.0;
                                    if(charidx>=577.0) return 178.0;
                                }else{
                                    if(charidx>=576.0) return 27.0;
                                    if(charidx>=575.0) return 26.0;
                                }
                            }
                        }
                    }
                }
            }else{
                if(charidx>=534.0){
                    if(charidx>=554.0){
                        if(charidx>=564.0){
                            if(charidx>=569.0){
                                if(charidx>=572.0){
                                    if(charidx>=574.0) return 248.0;
                                    if(charidx>=573.0) return 27.0;
                                    if(charidx>=572.0) return 3.0;
                                }else{
                                    if(charidx>=571.0) return 59.0;
                                    if(charidx>=570.0) return 178.0;
                                    if(charidx>=569.0) return 211.0;
                                }
                            }else{
                                if(charidx>=566.0){
                                    if(charidx>=568.0) return 3.0;
                                    if(charidx>=567.0) return 21.0;
                                    if(charidx>=566.0) return 51.0;
                                }else{
                                    if(charidx>=565.0) return 0.0;
                                    if(charidx>=564.0) return 178.0;
                                }
                            }
                        }else{
                            if(charidx>=559.0){
                                if(charidx>=561.0){
                                    if(charidx>=563.0) return 123.0;
                                    if(charidx>=562.0) return 114.0;
                                    if(charidx>=561.0) return 178.0;
                                }else{
                                    if(charidx>=560.0) return 0.0;
                                    if(charidx>=559.0) return 211.0;
                                }
                            }else{
                                if(charidx>=556.0){
                                    if(charidx>=558.0) return 26.0;
                                    if(charidx>=557.0) return 0.0;
                                    if(charidx>=556.0) return 30.0;
                                }else{
                                    if(charidx>=555.0) return 27.0;
                                    if(charidx>=554.0) return 114.0;
                                }
                            }
                        }
                    }else{
                        if(charidx>=544.0){
                            if(charidx>=549.0){
                                if(charidx>=551.0){
                                    if(charidx>=553.0) return 211.0;
                                    if(charidx>=551.0) return 0.0;
                                }else{
                                    if(charidx>=549.0) return 0.0;
                                }
                            }else{
                                if(charidx>=546.0){
                                    if(charidx>=546.0) return 0.0;
                                }else{
                                    if(charidx>=545.0) return 0.0;
                                    if(charidx>=544.0) return 258.0;
                                }
                            }
                        }else{
                            if(charidx>=539.0){
                                if(charidx>=541.0){
                                    if(charidx>=543.0) return 178.0;
                                    if(charidx>=542.0) return 11.0;
                                    if(charidx>=541.0) return 187.0;
                                }else{
                                    if(charidx>=540.0) return 15.0;
                                    if(charidx>=539.0) return 187.0;
                                }
                            }else{
                                if(charidx>=536.0){
                                    if(charidx>=538.0) return 11.0;
                                    if(charidx>=537.0) return 21.0;
                                    if(charidx>=536.0) return 51.0;
                                }else{
                                    if(charidx>=535.0) return 11.0;
                                    if(charidx>=534.0) return 27.0;
                                }
                            }
                        }
                    }
                }else{
                    if(charidx>=513.0){
                        if(charidx>=523.0){
                            if(charidx>=528.0){
                                if(charidx>=531.0){
                                    if(charidx>=533.0) return 19.0;
                                    if(charidx>=532.0) return 0.0;
                                    if(charidx>=531.0) return 15.0;
                                }else{
                                    if(charidx>=530.0) return 26.0;
                                    if(charidx>=529.0) return 178.0;
                                    if(charidx>=528.0) return 11.0;
                                }
                            }else{
                                if(charidx>=525.0){
                                    if(charidx>=527.0) return 123.0;
                                    if(charidx>=526.0) return 26.0;
                                    if(charidx>=525.0) return 91.0;
                                }else{
                                    if(charidx>=524.0) return 0.0;
                                    if(charidx>=523.0) return 11.0;
                                }
                            }
                        }else{
                            if(charidx>=518.0){
                                if(charidx>=520.0){
                                    if(charidx>=522.0) return 21.0;
                                    if(charidx>=521.0) return 0.0;
                                    if(charidx>=520.0) return 178.0;
                                }else{
                                    if(charidx>=519.0) return 59.0;
                                    if(charidx>=518.0) return 187.0;
                                }
                            }else{
                                if(charidx>=515.0){
                                    if(charidx>=517.0) return 118.0;
                                    if(charidx>=516.0) return 0.0;
                                    if(charidx>=515.0) return 3.0;
                                }else{
                                    if(charidx>=514.0) return 187.0;
                                    if(charidx>=513.0) return 26.0;
                                }
                            }
                        }
                    }else{
                        if(charidx>=503.0){
                            if(charidx>=508.0){
                                if(charidx>=510.0){
                                    if(charidx>=512.0) return 187.0;
                                    if(charidx>=511.0) return 11.0;
                                    if(charidx>=510.0) return 0.0;
                                }else{
                                    if(charidx>=509.0) return 211.0;
                                    if(charidx>=508.0) return 178.0;
                                }
                            }else{
                                if(charidx>=505.0){
                                    if(charidx>=507.0) return 21.0;
                                    if(charidx>=506.0) return 242.0;
                                    if(charidx>=505.0) return 0.0;
                                }else{
                                    if(charidx>=504.0) return 187.0;
                                    if(charidx>=503.0) return 219.0;
                                }
                            }
                        }else{
                            if(charidx>=498.0){
                                if(charidx>=500.0){
                                    if(charidx>=502.0) return 11.0;
                                    if(charidx>=501.0) return 123.0;
                                    if(charidx>=500.0) return 35.0;
                                }else{
                                    if(charidx>=499.0) return 16.0;
                                    if(charidx>=498.0) return 187.0;
                                }
                            }else{
                                if(charidx>=495.0){
                                    if(charidx>=497.0) return 187.0;
                                    if(charidx>=496.0) return 35.0;
                                    if(charidx>=495.0) return 51.0;
                                }else{
                                    if(charidx>=494.0) return 0.0;
                                    if(charidx>=493.0) return 15.0;
                                }
                            }
                        }
                    }
                }
            }
        }else{
            if(charidx>=411.0){
                if(charidx>=452.0){
                    if(charidx>=472.0){
                        if(charidx>=482.0){
                            if(charidx>=487.0){
                                if(charidx>=490.0){
                                    if(charidx>=492.0) return 27.0;
                                    if(charidx>=491.0) return 3.0;
                                    if(charidx>=490.0) return 51.0;
                                }else{
                                    if(charidx>=489.0) return 0.0;
                                    if(charidx>=488.0) return 123.0;
                                    if(charidx>=487.0) return 211.0;
                                }
                            }else{
                                if(charidx>=484.0){
                                    if(charidx>=486.0) return 234.0;
                                    if(charidx>=485.0) return 0.0;
                                    if(charidx>=484.0) return 187.0;
                                }else{
                                    if(charidx>=483.0) return 114.0;
                                    if(charidx>=482.0) return 178.0;
                                }
                            }
                        }else{
                            if(charidx>=477.0){
                                if(charidx>=479.0){
                                    if(charidx>=481.0) return 0.0;
                                    if(charidx>=480.0) return 11.0;
                                    if(charidx>=479.0) return 21.0;
                                }else{
                                    if(charidx>=478.0) return 0.0;
                                    if(charidx>=477.0) return 248.0;
                                }
                            }else{
                                if(charidx>=474.0){
                                    if(charidx>=476.0) return 11.0;
                                    if(charidx>=475.0) return 123.0;
                                    if(charidx>=474.0) return 114.0;
                                }else{
                                    if(charidx>=473.0) return 0.0;
                                    if(charidx>=472.0) return 218.0;
                                }
                            }
                        }
                    }else{
                        if(charidx>=462.0){
                            if(charidx>=467.0){
                                if(charidx>=469.0){
                                    if(charidx>=471.0) return 242.0;
                                    if(charidx>=470.0) return 0.0;
                                    if(charidx>=469.0) return 178.0;
                                }else{
                                    if(charidx>=468.0) return 211.0;
                                    if(charidx>=467.0) return 123.0;
                                }
                            }else{
                                if(charidx>=464.0){
                                    if(charidx>=466.0) return 19.0;
                                    if(charidx>=465.0) return 218.0;
                                    if(charidx>=464.0) return 123.0;
                                }else{
                                    if(charidx>=463.0) return 3.0;
                                    if(charidx>=462.0) return 0.0;
                                }
                            }
                        }else{
                            if(charidx>=457.0){
                                if(charidx>=459.0){
                                    if(charidx>=461.0) return 211.0;
                                    if(charidx>=460.0) return 218.0;
                                    if(charidx>=459.0) return 18.0;
                                }else{
                                    if(charidx>=458.0) return 27.0;
                                    if(charidx>=457.0) return 59.0;
                                }
                            }else{
                                if(charidx>=454.0){
                                    if(charidx>=456.0) return 0.0;
                                    if(charidx>=454.0) return 18.0;
                                }else{
                                    if(charidx>=453.0) return 123.0;
                                    if(charidx>=452.0) return 0.0;
                                }
                            }
                        }
                    }
                }else{
                    if(charidx>=431.0){
                        if(charidx>=441.0){
                            if(charidx>=446.0){
                                if(charidx>=449.0){
                                    if(charidx>=449.0) return 0.0;
                                }else{
                                    if(charidx>=448.0) return 0.0;
                                    if(charidx>=447.0) return 258.0;
                                    if(charidx>=446.0) return 248.0;
                                }
                            }else{
                                if(charidx>=443.0){
                                    if(charidx>=445.0) return 187.0;
                                    if(charidx>=444.0) return 11.0;
                                    if(charidx>=443.0) return 26.0;
                                }else{
                                    if(charidx>=442.0) return 178.0;
                                    if(charidx>=441.0) return 0.0;
                                }
                            }
                        }else{
                            if(charidx>=436.0){
                                if(charidx>=438.0){
                                    if(charidx>=440.0) return 218.0;
                                    if(charidx>=439.0) return 123.0;
                                    if(charidx>=438.0) return 178.0;
                                }else{
                                    if(charidx>=437.0) return 211.0;
                                    if(charidx>=436.0) return 0.0;
                                }
                            }else{
                                if(charidx>=433.0){
                                    if(charidx>=435.0) return 0.0;
                                    if(charidx>=434.0) return 258.0;
                                    if(charidx>=433.0) return 178.0;
                                }else{
                                    if(charidx>=432.0) return 21.0;
                                    if(charidx>=431.0) return 0.0;
                                }
                            }
                        }
                    }else{
                        if(charidx>=421.0){
                            if(charidx>=426.0){
                                if(charidx>=428.0){
                                    if(charidx>=430.0) return 178.0;
                                    if(charidx>=429.0) return 27.0;
                                    if(charidx>=428.0) return 11.0;
                                }else{
                                    if(charidx>=427.0) return 0.0;
                                    if(charidx>=426.0) return 211.0;
                                }
                            }else{
                                if(charidx>=423.0){
                                    if(charidx>=425.0) return 21.0;
                                    if(charidx>=424.0) return 0.0;
                                    if(charidx>=423.0) return 211.0;
                                }else{
                                    if(charidx>=422.0) return 21.0;
                                    if(charidx>=421.0) return 114.0;
                                }
                            }
                        }else{
                            if(charidx>=416.0){
                                if(charidx>=418.0){
                                    if(charidx>=420.0) return 178.0;
                                    if(charidx>=419.0) return 0.0;
                                    if(charidx>=418.0) return 178.0;
                                }else{
                                    if(charidx>=417.0) return 26.0;
                                    if(charidx>=416.0) return 242.0;
                                }
                            }else{
                                if(charidx>=413.0){
                                    if(charidx>=415.0) return 0.0;
                                    if(charidx>=414.0) return 187.0;
                                    if(charidx>=413.0) return 15.0;
                                }else{
                                    if(charidx>=412.0) return 27.0;
                                    if(charidx>=411.0) return 211.0;
                                }
                            }
                        }
                    }
                }
            }else{
                if(charidx>=370.0){
                    if(charidx>=390.0){
                        if(charidx>=400.0){
                            if(charidx>=405.0){
                                if(charidx>=408.0){
                                    if(charidx>=410.0) return 187.0;
                                    if(charidx>=409.0) return 30.0;
                                    if(charidx>=408.0) return 123.0;
                                }else{
                                    if(charidx>=407.0) return 0.0;
                                    if(charidx>=406.0) return 219.0;
                                    if(charidx>=405.0) return 11.0;
                                }
                            }else{
                                if(charidx>=402.0){
                                    if(charidx>=404.0) return 21.0;
                                    if(charidx>=403.0) return 114.0;
                                    if(charidx>=402.0) return 178.0;
                                }else{
                                    if(charidx>=401.0) return 187.0;
                                    if(charidx>=400.0) return 15.0;
                                }
                            }
                        }else{
                            if(charidx>=395.0){
                                if(charidx>=397.0){
                                    if(charidx>=399.0) return 27.0;
                                    if(charidx>=398.0) return 211.0;
                                    if(charidx>=397.0) return 0.0;
                                }else{
                                    if(charidx>=396.0) return 187.0;
                                    if(charidx>=395.0) return 118.0;
                                }
                            }else{
                                if(charidx>=392.0){
                                    if(charidx>=394.0) return 123.0;
                                    if(charidx>=393.0) return 15.0;
                                    if(charidx>=392.0) return 0.0;
                                }else{
                                    if(charidx>=391.0) return 27.0;
                                    if(charidx>=390.0) return 178.0;
                                }
                            }
                        }
                    }else{
                        if(charidx>=380.0){
                            if(charidx>=385.0){
                                if(charidx>=387.0){
                                    if(charidx>=389.0) return 0.0;
                                    if(charidx>=388.0) return 187.0;
                                    if(charidx>=387.0) return 3.0;
                                }else{
                                    if(charidx>=386.0) return 187.0;
                                    if(charidx>=385.0) return 114.0;
                                }
                            }else{
                                if(charidx>=382.0){
                                    if(charidx>=384.0) return 0.0;
                                    if(charidx>=383.0) return 187.0;
                                    if(charidx>=382.0) return 3.0;
                                }else{
                                    if(charidx>=381.0) return 123.0;
                                    if(charidx>=380.0) return 0.0;
                                }
                            }
                        }else{
                            if(charidx>=375.0){
                                if(charidx>=377.0){
                                    if(charidx>=379.0) return 187.0;
                                    if(charidx>=378.0) return 30.0;
                                    if(charidx>=377.0) return 0.0;
                                }else{
                                    if(charidx>=375.0) return 0.0;
                                }
                            }else{
                                if(charidx>=372.0){
                                    if(charidx>=372.0) return 0.0;
                                }else{
                                    if(charidx>=371.0) return 0.0;
                                    if(charidx>=370.0) return 0.0;
                                }
                            }
                        }
                    }
                }else{
                    if(charidx>=349.0){
                        if(charidx>=359.0){
                            if(charidx>=364.0){
                                if(charidx>=367.0){
                                    if(charidx>=367.0) return 0.0;
                                }else{
                                    if(charidx>=364.0) return 0.0;
                                }
                            }else{
                                if(charidx>=361.0){
                                    if(charidx>=363.0) return 256.0;
                                    if(charidx>=362.0) return 218.0;
                                    if(charidx>=361.0) return 3.0;
                                }else{
                                    if(charidx>=360.0) return 178.0;
                                    if(charidx>=359.0) return 187.0;
                                }
                            }
                        }else{
                            if(charidx>=354.0){
                                if(charidx>=356.0){
                                    if(charidx>=358.0) return 15.0;
                                    if(charidx>=357.0) return 27.0;
                                    if(charidx>=356.0) return 187.0;
                                }else{
                                    if(charidx>=355.0) return 219.0;
                                    if(charidx>=354.0) return 0.0;
                                }
                            }else{
                                if(charidx>=351.0){
                                    if(charidx>=353.0) return 187.0;
                                    if(charidx>=352.0) return 3.0;
                                    if(charidx>=351.0) return 27.0;
                                }else{
                                    if(charidx>=350.0) return 15.0;
                                    if(charidx>=349.0) return 0.0;
                                }
                            }
                        }
                    }else{
                        if(charidx>=339.0){
                            if(charidx>=344.0){
                                if(charidx>=346.0){
                                    if(charidx>=348.0) return 59.0;
                                    if(charidx>=347.0) return 26.0;
                                    if(charidx>=346.0) return 0.0;
                                }else{
                                    if(charidx>=345.0) return 185.0;
                                    if(charidx>=344.0) return 187.0;
                                }
                            }else{
                                if(charidx>=341.0){
                                    if(charidx>=343.0) return 3.0;
                                    if(charidx>=342.0) return 0.0;
                                    if(charidx>=341.0) return 211.0;
                                }else{
                                    if(charidx>=340.0) return 4.0;
                                    if(charidx>=339.0) return 178.0;
                                }
                            }
                        }else{
                            if(charidx>=334.0){
                                if(charidx>=336.0){
                                    if(charidx>=338.0) return 187.0;
                                    if(charidx>=337.0) return 162.0;
                                    if(charidx>=336.0) return 0.0;
                                }else{
                                    if(charidx>=335.0) return 0.0;
                                    if(charidx>=334.0) return 0.0;
                                }
                            }else{
                                if(charidx>=331.0){
                                    if(charidx>=331.0) return 0.0;
                                }else{
                                    if(charidx>=329.0) return 0.0;
                                }
                            }
                        }
                    }
                }
            }
        }
    }else{
        if(charidx>=164.0){
            if(charidx>=246.0){
                if(charidx>=287.0){
                    if(charidx>=308.0){
                        if(charidx>=318.0){
                            if(charidx>=323.0){
                                if(charidx>=326.0){
                                    if(charidx>=326.0) return 0.0;
                                }else{
                                    if(charidx>=325.0) return 0.0;
                                    if(charidx>=324.0) return 258.0;
                                    if(charidx>=323.0) return 234.0;
                                }
                            }else{
                                if(charidx>=320.0){
                                    if(charidx>=322.0) return 235.0;
                                    if(charidx>=321.0) return 218.0;
                                    if(charidx>=320.0) return 0.0;
                                }else{
                                    if(charidx>=318.0) return 0.0;
                                }
                            }
                        }else{
                            if(charidx>=313.0){
                                if(charidx>=315.0){
                                    if(charidx>=317.0) return 0.0;
                                    if(charidx>=316.0) return 248.0;
                                    if(charidx>=315.0) return 11.0;
                                }else{
                                    if(charidx>=314.0) return 123.0;
                                    if(charidx>=313.0) return 0.0;
                                }
                            }else{
                                if(charidx>=310.0){
                                    if(charidx>=312.0) return 218.0;
                                    if(charidx>=311.0) return 187.0;
                                    if(charidx>=310.0) return 218.0;
                                }else{
                                    if(charidx>=309.0) return 15.0;
                                    if(charidx>=308.0) return 185.0;
                                }
                            }
                        }
                    }else{
                        if(charidx>=297.0){
                            if(charidx>=302.0){
                                if(charidx>=305.0){
                                    if(charidx>=307.0) return 3.0;
                                    if(charidx>=306.0) return 187.0;
                                    if(charidx>=305.0) return 218.0;
                                }else{
                                    if(charidx>=304.0) return 0.0;
                                    if(charidx>=303.0) return 21.0;
                                    if(charidx>=302.0) return 26.0;
                                }
                            }else{
                                if(charidx>=299.0){
                                    if(charidx>=301.0) return 0.0;
                                    if(charidx>=300.0) return 118.0;
                                    if(charidx>=299.0) return 178.0;
                                }else{
                                    if(charidx>=298.0) return 11.0;
                                    if(charidx>=297.0) return 187.0;
                                }
                            }
                        }else{
                            if(charidx>=292.0){
                                if(charidx>=294.0){
                                    if(charidx>=296.0) return 18.0;
                                    if(charidx>=295.0) return 21.0;
                                    if(charidx>=294.0) return 211.0;
                                }else{
                                    if(charidx>=293.0) return 0.0;
                                    if(charidx>=292.0) return 178.0;
                                }
                            }else{
                                if(charidx>=289.0){
                                    if(charidx>=291.0) return 123.0;
                                    if(charidx>=290.0) return 211.0;
                                    if(charidx>=289.0) return 187.0;
                                }else{
                                    if(charidx>=288.0) return 11.0;
                                    if(charidx>=287.0) return 187.0;
                                }
                            }
                        }
                    }
                }else{
                    if(charidx>=266.0){
                        if(charidx>=276.0){
                            if(charidx>=281.0){
                                if(charidx>=284.0){
                                    if(charidx>=286.0) return 19.0;
                                    if(charidx>=285.0) return 211.0;
                                    if(charidx>=284.0) return 0.0;
                                }else{
                                    if(charidx>=283.0) return 123.0;
                                    if(charidx>=282.0) return 178.0;
                                    if(charidx>=281.0) return 19.0;
                                }
                            }else{
                                if(charidx>=278.0){
                                    if(charidx>=280.0) return 187.0;
                                    if(charidx>=279.0) return 11.0;
                                    if(charidx>=278.0) return 0.0;
                                }else{
                                    if(charidx>=277.0) return 218.0;
                                    if(charidx>=276.0) return 187.0;
                                }
                            }
                        }else{
                            if(charidx>=271.0){
                                if(charidx>=273.0){
                                    if(charidx>=275.0) return 118.0;
                                    if(charidx>=274.0) return 11.0;
                                    if(charidx>=273.0) return 27.0;
                                }else{
                                    if(charidx>=272.0) return 15.0;
                                    if(charidx>=271.0) return 0.0;
                                }
                            }else{
                                if(charidx>=268.0){
                                    if(charidx>=270.0) return 187.0;
                                    if(charidx>=269.0) return 11.0;
                                    if(charidx>=268.0) return 21.0;
                                }else{
                                    if(charidx>=267.0) return 3.0;
                                    if(charidx>=266.0) return 123.0;
                                }
                            }
                        }
                    }else{
                        if(charidx>=256.0){
                            if(charidx>=261.0){
                                if(charidx>=263.0){
                                    if(charidx>=265.0) return 248.0;
                                    if(charidx>=264.0) return 11.0;
                                    if(charidx>=263.0) return 123.0;
                                }else{
                                    if(charidx>=262.0) return 15.0;
                                    if(charidx>=261.0) return 0.0;
                                }
                            }else{
                                if(charidx>=258.0){
                                    if(charidx>=258.0) return 0.0;
                                }else{
                                    if(charidx>=257.0) return 0.0;
                                    if(charidx>=256.0) return 0.0;
                                }
                            }
                        }else{
                            if(charidx>=251.0){
                                if(charidx>=253.0){
                                    if(charidx>=253.0) return 0.0;
                                }else{
                                    if(charidx>=251.0) return 0.0;
                                }
                            }else{
                                if(charidx>=248.0){
                                    if(charidx>=249.0) return 0.0;
                                    if(charidx>=248.0) return 187.0;
                                }else{
                                    if(charidx>=247.0) return 19.0;
                                    if(charidx>=246.0) return 11.0;
                                }
                            }
                        }
                    }
                }
            }else{
                if(charidx>=205.0){
                    if(charidx>=225.0){
                        if(charidx>=235.0){
                            if(charidx>=240.0){
                                if(charidx>=243.0){
                                    if(charidx>=245.0) return 187.0;
                                    if(charidx>=244.0) return 19.0;
                                    if(charidx>=243.0) return 211.0;
                                }else{
                                    if(charidx>=242.0) return 187.0;
                                    if(charidx>=241.0) return 11.0;
                                    if(charidx>=240.0) return 21.0;
                                }
                            }else{
                                if(charidx>=237.0){
                                    if(charidx>=239.0) return 15.0;
                                    if(charidx>=238.0) return 26.0;
                                    if(charidx>=237.0) return 18.0;
                                }else{
                                    if(charidx>=236.0) return 0.0;
                                    if(charidx>=235.0) return 258.0;
                                }
                            }
                        }else{
                            if(charidx>=230.0){
                                if(charidx>=232.0){
                                    if(charidx>=234.0) return 187.0;
                                    if(charidx>=233.0) return 26.0;
                                    if(charidx>=232.0) return 21.0;
                                }else{
                                    if(charidx>=231.0) return 178.0;
                                    if(charidx>=230.0) return 11.0;
                                }
                            }else{
                                if(charidx>=227.0){
                                    if(charidx>=229.0) return 187.0;
                                    if(charidx>=228.0) return 19.0;
                                    if(charidx>=227.0) return 11.0;
                                }else{
                                    if(charidx>=226.0) return 21.0;
                                    if(charidx>=225.0) return 0.0;
                                }
                            }
                        }
                    }else{
                        if(charidx>=215.0){
                            if(charidx>=220.0){
                                if(charidx>=222.0){
                                    if(charidx>=224.0) return 90.0;
                                    if(charidx>=223.0) return 16.0;
                                    if(charidx>=222.0) return 200.0;
                                }else{
                                    if(charidx>=221.0) return 123.0;
                                    if(charidx>=220.0) return 219.0;
                                }
                            }else{
                                if(charidx>=217.0){
                                    if(charidx>=219.0) return 3.0;
                                    if(charidx>=218.0) return 123.0;
                                    if(charidx>=217.0) return 219.0;
                                }else{
                                    if(charidx>=216.0) return 0.0;
                                    if(charidx>=215.0) return 258.0;
                                }
                            }
                        }else{
                            if(charidx>=210.0){
                                if(charidx>=212.0){
                                    if(charidx>=214.0) return 11.0;
                                    if(charidx>=213.0) return 123.0;
                                    if(charidx>=212.0) return 19.0;
                                }else{
                                    if(charidx>=211.0) return 0.0;
                                    if(charidx>=210.0) return 26.0;
                                }
                            }else{
                                if(charidx>=207.0){
                                    if(charidx>=209.0) return 27.0;
                                    if(charidx>=208.0) return 218.0;
                                    if(charidx>=207.0) return 16.0;
                                }else{
                                    if(charidx>=206.0) return 211.0;
                                    if(charidx>=205.0) return 234.0;
                                }
                            }
                        }
                    }
                }else{
                    if(charidx>=184.0){
                        if(charidx>=194.0){
                            if(charidx>=199.0){
                                if(charidx>=202.0){
                                    if(charidx>=204.0) return 187.0;
                                    if(charidx>=203.0) return 248.0;
                                    if(charidx>=202.0) return 0.0;
                                }else{
                                    if(charidx>=201.0) return 15.0;
                                    if(charidx>=200.0) return 219.0;
                                    if(charidx>=199.0) return 26.0;
                                }
                            }else{
                                if(charidx>=196.0){
                                    if(charidx>=198.0) return 19.0;
                                    if(charidx>=197.0) return 0.0;
                                    if(charidx>=196.0) return 19.0;
                                }else{
                                    if(charidx>=195.0) return 19.0;
                                    if(charidx>=194.0) return 26.0;
                                }
                            }
                        }else{
                            if(charidx>=189.0){
                                if(charidx>=191.0){
                                    if(charidx>=193.0) return 15.0;
                                    if(charidx>=192.0) return 19.0;
                                    if(charidx>=191.0) return 0.0;
                                }else{
                                    if(charidx>=190.0) return 211.0;
                                    if(charidx>=189.0) return 35.0;
                                }
                            }else{
                                if(charidx>=186.0){
                                    if(charidx>=188.0) return 242.0;
                                    if(charidx>=187.0) return 0.0;
                                    if(charidx>=186.0) return 218.0;
                                }else{
                                    if(charidx>=185.0) return 178.0;
                                    if(charidx>=184.0) return 3.0;
                                }
                            }
                        }
                    }else{
                        if(charidx>=174.0){
                            if(charidx>=179.0){
                                if(charidx>=181.0){
                                    if(charidx>=183.0) return 123.0;
                                    if(charidx>=182.0) return 59.0;
                                    if(charidx>=181.0) return 178.0;
                                }else{
                                    if(charidx>=180.0) return 123.0;
                                    if(charidx>=179.0) return 0.0;
                                }
                            }else{
                                if(charidx>=176.0){
                                    if(charidx>=178.0) return 21.0;
                                    if(charidx>=176.0) return 11.0;
                                }else{
                                    if(charidx>=175.0) return 123.0;
                                    if(charidx>=174.0) return 0.0;
                                }
                            }
                        }else{
                            if(charidx>=169.0){
                                if(charidx>=171.0){
                                    if(charidx>=173.0) return 178.0;
                                    if(charidx>=171.0) return 187.0;
                                }else{
                                    if(charidx>=170.0) return 3.0;
                                    if(charidx>=169.0) return 219.0;
                                }
                            }else{
                                if(charidx>=166.0){
                                    if(charidx>=168.0) return 0.0;
                                    if(charidx>=167.0) return 187.0;
                                    if(charidx>=166.0) return 30.0;
                                }else{
                                    if(charidx>=164.0) return 0.0;
                                }
                            }
                        }
                    }
                }
            }
        }else{
            if(charidx>=82.0){
                if(charidx>=123.0){
                    if(charidx>=143.0){
                        if(charidx>=153.0){
                            if(charidx>=158.0){
                                if(charidx>=161.0){
                                    if(charidx>=161.0) return 0.0;
                                }else{
                                    if(charidx>=160.0) return 0.0;
                                    if(charidx>=159.0) return 0.0;
                                    if(charidx>=158.0) return 0.0;
                                }
                            }else{
                                if(charidx>=155.0){
                                    if(charidx>=155.0) return 0.0;
                                }else{
                                    if(charidx>=154.0) return 0.0;
                                    if(charidx>=153.0) return 258.0;
                                }
                            }
                        }else{
                            if(charidx>=148.0){
                                if(charidx>=150.0){
                                    if(charidx>=152.0) return 219.0;
                                    if(charidx>=151.0) return 21.0;
                                    if(charidx>=150.0) return 3.0;
                                }else{
                                    if(charidx>=149.0) return 123.0;
                                    if(charidx>=148.0) return 200.0;
                                }
                            }else{
                                if(charidx>=145.0){
                                    if(charidx>=147.0) return 0.0;
                                    if(charidx>=146.0) return 211.0;
                                    if(charidx>=145.0) return 123.0;
                                }else{
                                    if(charidx>=144.0) return 30.0;
                                    if(charidx>=143.0) return 0.0;
                                }
                            }
                        }
                    }else{
                        if(charidx>=133.0){
                            if(charidx>=138.0){
                                if(charidx>=140.0){
                                    if(charidx>=142.0) return 248.0;
                                    if(charidx>=141.0) return 27.0;
                                    if(charidx>=140.0) return 15.0;
                                }else{
                                    if(charidx>=139.0) return 179.0;
                                    if(charidx>=138.0) return 0.0;
                                }
                            }else{
                                if(charidx>=135.0){
                                    if(charidx>=135.0) return 0.0;
                                }else{
                                    if(charidx>=134.0) return 243.0;
                                    if(charidx>=133.0) return 72.0;
                                }
                            }
                        }else{
                            if(charidx>=128.0){
                                if(charidx>=130.0){
                                    if(charidx>=132.0) return 235.0;
                                    if(charidx>=131.0) return 185.0;
                                    if(charidx>=130.0) return 0.0;
                                }else{
                                    if(charidx>=129.0) return 187.0;
                                    if(charidx>=128.0) return 19.0;
                                }
                            }else{
                                if(charidx>=125.0){
                                    if(charidx>=127.0) return 11.0;
                                    if(charidx>=126.0) return 21.0;
                                    if(charidx>=125.0) return 211.0;
                                }else{
                                    if(charidx>=124.0) return 0.0;
                                    if(charidx>=123.0) return 187.0;
                                }
                            }
                        }
                    }
                }else{
                    if(charidx>=102.0){
                        if(charidx>=112.0){
                            if(charidx>=117.0){
                                if(charidx>=120.0){
                                    if(charidx>=122.0) return 19.0;
                                    if(charidx>=121.0) return 11.0;
                                    if(charidx>=120.0) return 123.0;
                                }else{
                                    if(charidx>=119.0) return 21.0;
                                    if(charidx>=117.0) return 18.0;
                                }
                            }else{
                                if(charidx>=114.0){
                                    if(charidx>=116.0) return 123.0;
                                    if(charidx>=115.0) return 0.0;
                                    if(charidx>=114.0) return 18.0;
                                }else{
                                    if(charidx>=113.0) return 187.0;
                                    if(charidx>=112.0) return 242.0;
                                }
                            }
                        }else{
                            if(charidx>=107.0){
                                if(charidx>=109.0){
                                    if(charidx>=111.0) return 187.0;
                                    if(charidx>=110.0) return 3.0;
                                    if(charidx>=109.0) return 0.0;
                                }else{
                                    if(charidx>=108.0) return 187.0;
                                    if(charidx>=107.0) return 114.0;
                                }
                            }else{
                                if(charidx>=104.0){
                                    if(charidx>=106.0) return 178.0;
                                    if(charidx>=105.0) return 0.0;
                                    if(charidx>=104.0) return 219.0;
                                }else{
                                    if(charidx>=103.0) return 11.0;
                                    if(charidx>=102.0) return 21.0;
                                }
                            }
                        }
                    }else{
                        if(charidx>=92.0){
                            if(charidx>=97.0){
                                if(charidx>=99.0){
                                    if(charidx>=101.0) return 118.0;
                                    if(charidx>=100.0) return 19.0;
                                    if(charidx>=99.0) return 27.0;
                                }else{
                                    if(charidx>=98.0) return 3.0;
                                    if(charidx>=97.0) return 4.0;
                                }
                            }else{
                                if(charidx>=94.0){
                                    if(charidx>=96.0) return 0.0;
                                    if(charidx>=95.0) return 187.0;
                                    if(charidx>=94.0) return 11.0;
                                }else{
                                    if(charidx>=93.0) return 21.0;
                                    if(charidx>=92.0) return 18.0;
                                }
                            }
                        }else{
                            if(charidx>=87.0){
                                if(charidx>=89.0){
                                    if(charidx>=91.0) return 19.0;
                                    if(charidx>=90.0) return 11.0;
                                    if(charidx>=89.0) return 21.0;
                                }else{
                                    if(charidx>=87.0) return 0.0;
                                }
                            }else{
                                if(charidx>=84.0){
                                    if(charidx>=84.0) return 0.0;
                                }else{
                                    if(charidx>=82.0) return 0.0;
                                }
                            }
                        }
                    }
                }
            }else{
                if(charidx>=41.0){
                    if(charidx>=61.0){
                        if(charidx>=71.0){
                            if(charidx>=76.0){
                                if(charidx>=79.0){
                                    if(charidx>=80.0) return 0.0;
                                    if(charidx>=79.0) return 258.0;
                                }else{
                                    if(charidx>=78.0) return 3.0;
                                    if(charidx>=77.0) return 187.0;
                                    if(charidx>=76.0) return 26.0;
                                }
                            }else{
                                if(charidx>=73.0){
                                    if(charidx>=75.0) return 187.0;
                                    if(charidx>=74.0) return 0.0;
                                    if(charidx>=73.0) return 59.0;
                                }else{
                                    if(charidx>=72.0) return 26.0;
                                    if(charidx>=71.0) return 27.0;
                                }
                            }
                        }else{
                            if(charidx>=66.0){
                                if(charidx>=68.0){
                                    if(charidx>=70.0) return 3.0;
                                    if(charidx>=69.0) return 219.0;
                                    if(charidx>=68.0) return 27.0;
                                }else{
                                    if(charidx>=67.0) return 15.0;
                                    if(charidx>=66.0) return 187.0;
                                }
                            }else{
                                if(charidx>=63.0){
                                    if(charidx>=65.0) return 248.0;
                                    if(charidx>=64.0) return 0.0;
                                    if(charidx>=63.0) return 248.0;
                                }else{
                                    if(charidx>=62.0) return 187.0;
                                    if(charidx>=61.0) return 26.0;
                                }
                            }
                        }
                    }else{
                        if(charidx>=51.0){
                            if(charidx>=56.0){
                                if(charidx>=58.0){
                                    if(charidx>=60.0) return 27.0;
                                    if(charidx>=59.0) return 3.0;
                                    if(charidx>=58.0) return 59.0;
                                }else{
                                    if(charidx>=57.0) return 15.0;
                                    if(charidx>=56.0) return 21.0;
                                }
                            }else{
                                if(charidx>=53.0){
                                    if(charidx>=55.0) return 0.0;
                                    if(charidx>=54.0) return 178.0;
                                    if(charidx>=53.0) return 211.0;
                                }else{
                                    if(charidx>=52.0) return 27.0;
                                    if(charidx>=51.0) return 15.0;
                                }
                            }
                        }else{
                            if(charidx>=46.0){
                                if(charidx>=48.0){
                                    if(charidx>=50.0) return 0.0;
                                    if(charidx>=49.0) return 248.0;
                                    if(charidx>=48.0) return 11.0;
                                }else{
                                    if(charidx>=47.0) return 123.0;
                                    if(charidx>=46.0) return 0.0;
                                }
                            }else{
                                if(charidx>=43.0){
                                    if(charidx>=45.0) return 178.0;
                                    if(charidx>=44.0) return 211.0;
                                    if(charidx>=43.0) return 187.0;
                                }else{
                                    if(charidx>=42.0) return 30.0;
                                    if(charidx>=41.0) return 187.0;
                                }
                            }
                        }
                    }
                }else{
                    if(charidx>=20.0){
                        if(charidx>=30.0){
                            if(charidx>=35.0){
                                if(charidx>=38.0){
                                    if(charidx>=40.0) return 11.0;
                                    if(charidx>=39.0) return 0.0;
                                    if(charidx>=38.0) return 187.0;
                                }else{
                                    if(charidx>=37.0) return 114.0;
                                    if(charidx>=36.0) return 178.0;
                                    if(charidx>=35.0) return 0.0;
                                }
                            }else{
                                if(charidx>=32.0){
                                    if(charidx>=34.0) return 16.0;
                                    if(charidx>=33.0) return 0.0;
                                    if(charidx>=32.0) return 187.0;
                                }else{
                                    if(charidx>=31.0) return 11.0;
                                    if(charidx>=30.0) return 21.0;
                                }
                            }
                        }else{
                            if(charidx>=25.0){
                                if(charidx>=27.0){
                                    if(charidx>=29.0) return 18.0;
                                    if(charidx>=28.0) return 19.0;
                                    if(charidx>=27.0) return 11.0;
                                }else{
                                    if(charidx>=26.0) return 21.0;
                                    if(charidx>=25.0) return 0.0;
                                }
                            }else{
                                if(charidx>=22.0){
                                    if(charidx>=24.0) return 187.0;
                                    if(charidx>=23.0) return 3.0;
                                    if(charidx>=22.0) return 123.0;
                                }else{
                                    if(charidx>=21.0) return 0.0;
                                    if(charidx>=20.0) return 187.0;
                                }
                            }
                        }
                    }else{
                        if(charidx>=10.0){
                            if(charidx>=15.0){
                                if(charidx>=17.0){
                                    if(charidx>=19.0) return 30.0;
                                    if(charidx>=17.0) return 0.0;
                                }else{
                                    if(charidx>=16.0) return 258.0;
                                    if(charidx>=15.0) return 248.0;
                                }
                            }else{
                                if(charidx>=12.0){
                                    if(charidx>=14.0) return 162.0;
                                    if(charidx>=13.0) return 35.0;
                                    if(charidx>=12.0) return 235.0;
                                }else{
                                    if(charidx>=11.0) return 30.0;
                                    if(charidx>=10.0) return 0.0;
                                }
                            }
                        }else{
                            if(charidx>=5.0){
                                if(charidx>=7.0){
                                    if(charidx>=9.0) return 235.0;
                                    if(charidx>=7.0) return 162.0;
                                }else{
                                    if(charidx>=6.0) return 179.0;
                                    if(charidx>=5.0) return 122.0;
                                }
                            }else{
                                if(charidx>=2.0){
                                    if(charidx>=2.0) return 0.0;
                                }else{
                                    if(charidx>=0.0) return 0.0;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return 0.0; //empty by default
} //get_seg_mask

// Camera and light prototypes

void do_cl_nop(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);
void do_cl_s_plain(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);
void do_cl_starwars(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);
void do_cl_s_zoom(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);
void do_cl_s_roto(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);
void do_cl_bold(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);
void do_cl_cyl(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);
void do_cl_tunnel(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);
void do_cl_s_sine(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);
void do_cl_twosided(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);
void do_cl_s_plasma(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);
void do_cl_cube(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);

void do_camera_light(in float partnum, in float charidx_frac,
                        out vec3 camera_pos,
                        out vec3 camera_look_at, out vec3 camera_up,
                        out float fovy_deg, out vec3 light_pos)
{   // Camera and light dispatcher
    if(partnum>=CYL) {

        if(partnum==CYL) {
            do_cl_cyl(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        if(partnum==TUNNEL) {
            do_cl_tunnel(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        if(partnum==S_SINE) {
            do_cl_s_sine(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        if(partnum==TWOSIDED) {
            do_cl_twosided(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        if(partnum==S_PLASMA) {
            do_cl_s_plasma(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        if(partnum==CUBE) {
            do_cl_cube(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        {
            camera_pos=vec3(0.0,0.0,10.0);    //default
            camera_look_at=vec3(0.0);
            camera_up=vec3(0.0, 1.0, 0.0);
            fovy_deg=45.0;
            light_pos=camera_pos;
        }
    } else {

        if(partnum==NOP) {
            do_cl_nop(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        if(partnum==S_PLAIN) {
            do_cl_s_plain(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        if(partnum==STARWARS) {
            do_cl_starwars(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        if(partnum==S_ZOOM) {
            do_cl_s_zoom(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        if(partnum==S_ROTO) {
            do_cl_s_roto(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        if(partnum==BOLD) {
            do_cl_bold(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        {
            camera_pos=vec3(0.0,0.0,10.0);    //default
            camera_look_at=vec3(0.0);
            camera_up=vec3(0.0, 1.0, 0.0);
            fovy_deg=45.0;
            light_pos=camera_pos;
        }
    }
} //do_camera_light

// --- ^^^ cutend ---
// }}}1

// UTIL ///////////////////////////////////////////////
// {{{1
mat4 transpose(in mat4 inMatrix)
{
    // Modified from
    // http://stackoverflow.com/a/18038495/2877364 by
    // http://stackoverflow.com/users/2507370/jeb
    vec4 i0 = inMatrix[0];
    vec4 i1 = inMatrix[1];
    vec4 i2 = inMatrix[2];
    vec4 i3 = inMatrix[3];

    vec4 o0 = vec4(i0.x, i1.x, i2.x, i3.x);
    vec4 o1 = vec4(i0.y, i1.y, i2.y, i3.y);
    vec4 o2 = vec4(i0.z, i1.z, i2.z, i3.z);
    vec4 o3 = vec4(i0.w, i1.w, i2.w, i3.w);

    mat4 outMatrix = mat4(o0, o1, o2, o3);

    return outMatrix;
}

void lookat(in vec3 in_eye, in vec3 in_ctr, in vec3 in_up,
            out mat4 view, out mat4 view_inv)
{
    // From Mesa glu.  Thanks to
    // http://learnopengl.com/#!Getting-started/Camera
    // and https://www.opengl.org/wiki/GluLookAt_code

    vec3 forward, side, up;

    forward=normalize(in_ctr-in_eye);
    up = in_up;
    side = normalize(cross(forward,up));
    up = cross(side,forward);   // already normalized since both inputs are
        //now side, up, and forward are orthonormal

    mat4 orient, where;

    // Note: in Mesa gluLookAt, a C matrix is used, so the indices
    // have to be swapped compared to that code.
    vec4 x4, y4, z4, w4;
    x4 = vec4(side,0);
    y4 = vec4(up,0);
    z4 = vec4(-forward,0);
    w4 = vec4(0,0,0,1);
    orient = transpose(mat4(x4, y4, z4, w4));

    where = mat4(1.0); //identity (1.0 diagonal matrix)
    where[3] = vec4(-in_eye, 1);

    view = (orient * where);

    // Compute the inverse for later
    view_inv = mat4(x4, y4, z4, -where[3]);
    view_inv[3][3] = 1.0;   // since -where[3].w == -1, not what we want
        // Per https://en.wikibooks.org/wiki/GLSL_Programming/Vertex_Transformations ,
        // M_{view->world}
} //lookat

void gluPerspective(in float fovy_deg, in float aspect,
                    in float near, in float far,
                    out mat4 proj, out mat4 proj_inv)
{   // from mesa glu-9.0.0/src/libutil/project.c.
    // Thanks to https://unspecified.wordpress.com/2012/06/21/calculating-the-gluperspective-matrix-and-other-opengl-matrix-maths/

    float fovy_rad = radians(fovy_deg);
    float dz = far-near;
    float sin_fovy = sin(fovy_rad);
    float cot_fovy = cos(fovy_rad) / sin_fovy;

    proj=mat4(0);
    //[col][row]
    proj[0][0] = cot_fovy / aspect;
    proj[1][1] = cot_fovy;

    proj[2][2] = -(far+near)/dz;
    proj[2][3] = -1.0;

    proj[3][2] = -2.0*near*far/dz;

    // Compute the inverse matrix.
    // http://bookofhook.com/mousepick.pdf
    float a = proj[0][0];
    float b = proj[1][1];
    float c = proj[2][2];
    float d = proj[3][2];
    float e = proj[2][3];

    proj_inv = mat4(0);
    proj_inv[0][0] = 1.0/a;
    proj_inv[1][1] = 1.0/b;
    proj_inv[3][2] = 1.0/e;
    proj_inv[2][3] = 1.0/d;
    proj_inv[3][3] = -c/(d*e);
} //gluPerspective

void compute_viewport(in float x, in float y, in float w, in float h,
                        out mat4 viewp, out mat4 viewp_inv)
{
    // See https://en.wikibooks.org/wiki/GLSL_Programming/Vertex_Transformations#Viewport_Transformation
    // Also mesa src/mesa/main/viewport.c:_mesa_get_viewport_xform()

    viewp = mat4(0);
    // Reminder: indexing is [col][row]
    viewp[0][0] = w/2.0;
    viewp[3][0] = x+w/2.0;

    viewp[1][1] = h/2.0;
    viewp[3][1] = y+h/2.0;

    // assumes n=0 and f=1,
    // which are the default for glDepthRange.
    viewp[2][2] = 0.5;  // actually 0.5 * (f-n);
    viewp[3][2] = 0.5;  // actually 0.5 * (n+f);

    viewp[3][3] = 1.0;

    //Invert.  Done by hand.
    viewp_inv = mat4(1.0);
    viewp_inv[0][0] = 2.0/w;    // x->x
    viewp_inv[3][0] = -1.0 - (2.0*x/w);

    viewp_inv[1][1] = 2.0/h;    // y->y
    viewp_inv[3][1] = -1.0 - (2.0*y/h);

    viewp_inv[2][2] = 2.0;      // z->z
    viewp_inv[3][2] = -1.0;

}  //compute_viewport

// https://www.opengl.org/wiki/Compute_eye_space_from_window_space

vec4 wts(in mat4 modelviewproj, in mat4 viewport,
                in vec3 pos)
{   // world to screen coordinates
    vec4 clipvertex = modelviewproj * vec4(pos,1.0);
    vec4 ndc = clipvertex/clipvertex.w;
    vec4 transformed = viewport * ndc;
    return transformed;
} //wts

// screen to world: http://bookofhook.com/mousepick.pdf
vec4 WorldRayFromScreenPoint(in vec2 scr_pt,
    in mat4 view_inv,
    in mat4 proj_inv,
    in mat4 viewp_inv)
{   // Returns world coords of a point on a ray passing through
    // the camera position and scr_pt.

    vec4 ndc = viewp_inv * vec4(scr_pt,0.0,1.0);
        // z=0.0 => it's a ray.  0 is an arbitrary choice in the
        // view volume.
        // w=1.0 => we don't need to undo the perspective divide.
        //      So clip coords == NDC

    vec4 view_coords = proj_inv * ndc;
        // At this point, z=0 will have become something in the
        // middle of the projection volume, somewhere between
        // near and far.
    view_coords = view_coords / view_coords.w;
        // Keepin' it real?  Not sure what happens if you skip this.
    //view_coords.w = 0.0;
        // Remove translation components.  Note that we
        // don't use this trick.
    vec4 world_ray_point = view_inv * view_coords;
        // Now scr_pt is on the ray through camera_pos and world_ray_point
    return world_ray_point;
} //WorldRayFromScreenPoint

vec3 hsv2rgb(vec3 c) {
    // by hughsk, from https://github.com/hughsk/glsl-hsv2rgb/blob/master/index.glsl .
    // All inputs range from 0 to 1.
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

// }}}1

// GEOMETRY HIT-TESTING ///////////////////////////////
// {{{1

// Plane-testing routines for cube.
vec4 HitXPlane(vec3 camera_pos, vec3 camera_dir, float plane_locn)
{   //returns .xyz = hit coords; .w = time of hit
    float hit_t = (plane_locn-camera_pos.x) / camera_dir.x;
    return vec4(camera_pos + hit_t*camera_dir, hit_t);
} //HitXPlane

vec4 HitYPlane(vec3 camera_pos, vec3 camera_dir, float plane_locn)
{   //returns .xyz = hit coords; .w = time of hit
    float hit_t = (plane_locn-camera_pos.y) / camera_dir.y;
    return vec4(camera_pos + hit_t*camera_dir, hit_t);
} //HitYPlane

vec4 HitZPlane(vec3 camera_pos, vec3 camera_dir, float plane_locn)
{   //returns .xyz = hit coords; .w = time of hit
    float hit_t = (plane_locn-camera_pos.z) / camera_dir.z;
    return vec4(camera_pos + hit_t*camera_dir, hit_t);
} //HitZPlane

// Faster routine for the special case of the main text
vec3 HitZZero(vec3 camera_pos, vec3 rayend)
{   // Find where the ray meets the z=0 plane.  The ray is
    // camera_pos + t*(rayend - camera_pos) per Hook.
    float hit_t = -camera_pos.z / (rayend.z - camera_pos.z);
    return (camera_pos + hit_t * (rayend-camera_pos));
} //HitZZero

// --- IsPointInRectXY ---
// All polys will be quads in the X-Y plane, Z=0.
// All quad edges are parallel to the X or Y axis.
// These quads are encoded in a vec4: (.x,.y) is the LL corner and
// (.z,.w) is the UR corner (coords (x,y)).

bool IsPointInRectXY(in vec4 poly_coords, in vec2 world_xy_of_point)
{
    // return true if world_xy_of_point is within the poly defined by
    // poly_coords in the Z=0 plane.
    // I can test in 2D rather than 3D because all the geometry
    // has z=0 and all the quads are planar.

    float x_test, y_test;
    x_test = step(poly_coords.x, world_xy_of_point.x) *
            (1.0 - step(poly_coords.z, world_xy_of_point.x));
        // step() is 1.0 if world.x >= poly_coords.x
        // 1-step() is 1.0 if world.x < poly_coords.z
    y_test = step(poly_coords.y, world_xy_of_point.y) *
            (1.0 - step(poly_coords.w, world_xy_of_point.y));

    return ( (x_test>=0.9) && (y_test >= 0.9) );
        // Not ==1.0 because these are floats!

} //IsPointInRectXY

vec4 cylinder_hit(float cyl_radius, vec3 camera_pos, vec3 ray_end)
{   // Hit-test a cylinder with infinite extent in X centered on the X-axis.
    // Ray starts at camera_pos and passes through ray_end.
    // Returns (xyz,\theta).  \theta is the angle in radians about the
    // X axis from +Y towards +X.  \theta=0 => +Y axis (Z=0).
    // Preconditions: camera_pos is within the cylinder, and
    // ray_end has nonzero .yz.

    // Based on http://stackoverflow.com/a/1084899/2877364 by
    // http://stackoverflow.com/users/111307/bobobobo

    // Test for hits projected to the YZ plane.
    vec2 E = camera_pos.yz;
    vec2 L = ray_end.yz;    // end of the ray
    vec2 d = L - E;         // direction of the ray
    vec2 C = vec2(0.0);     // cylinder is centered on X axis
    vec2 f = E-C;
    float r = cyl_radius;

    float a = dot(d, d);
    float b = 2.0*dot(f,d);
    float c = dot(f,f) - r*r;
    float discriminant = b*b-4.0*a*c;

    discriminant = sqrt(discriminant);

    // Since camera_pos is within the cylinder, we have what bobobobo describes
    // as an "ExitWound" condition.  The second root (+discriminant) is the one we want.
    float t2 = (-b + discriminant) / (2.0*a);

    // Now expand to 3D
    vec3 hit_point = camera_pos + t2*(ray_end-camera_pos);  // XYZ

    return vec4(hit_point, atan(hit_point.z, hit_point.y));
        // Right-handed rotation about X axis from +Y toward +Z

} //cylinder_hit

vec3 sphere_hit(vec3 camera_pos, vec3 rayend, vec3 center, float radius)
{   // return the point on a sphere centered at _center_ hit by the ray,
    // or (0,0,0) if no hit.
    // From http://gamedev.stackexchange.com/a/27758/51386 by
    // http://gamedev.stackexchange.com/users/5864/sam-hocevar
    vec3 dir = rayend-camera_pos;
    float a = dot(dir,dir);
    float b = 2.0 * dot(dir, camera_pos - center);
    float c = dot(center, center) + dot(camera_pos, camera_pos) -
        2.0 * dot(center, camera_pos) - radius*radius;

    // This from http://gamedev.stackexchange.com/q/27755/51386 by
    // http://gamedev.stackexchange.com/users/14718/fernacolo
    float discrim = b*b - 4.0*a*c;

    if(discrim < 0.0) {
        return vec3(0.0);   //no hit
    } else {
        float t = (-b - sqrt(discrim)) / (2.0*a);
        return camera_pos + t*dir;
    } //endif no hit else

} //sphere_hit

#define CUBEHIT_THLD (0.01)
bool cube_hit(in vec3 camera_pos, in vec3 rayend,
    out vec3 wc_pixel, out vec3 wc_normal,
    out vec3 ambient, out vec3 diffuse)
{
    // Check for intersection with a cube from (0,0,0) to (1,1,1).
    // Returns true iff hit.
    // Maybe check each axis in turn, get both ts, and see if either
    // is in range.

    bool did_hit = false;
    vec3 camera_dir = rayend - camera_pos;

    // Colors

    // X faces
    if(abs(camera_dir.x)>CUBEHIT_THLD) {    //Don't shoot parallel to X plane
        vec4 hitx0 = HitXPlane(camera_pos, camera_dir, 0.0);
            //coords and t of X=0 hit
        vec4 hitx1 = HitXPlane(camera_pos, camera_dir, 1.0);
        if(hitx0.w<hitx1.w) {   // check X=0 hit
            did_hit = IsPointInRectXY(vec4(0,0,1,1), hitx0.yz);
            wc_pixel = hitx0.xyz;
            wc_normal = vec3(-1.0, 0.0, 0.0);
                // The X=0 face has a normal pointing -X
            ambient = vec3(0.1, 0.0, 0.0);
            diffuse = vec3(0.5, 0.0, 0.0);
        } else {            // check X=1 hit
            did_hit = IsPointInRectXY(vec4(0,0,1,1), hitx1.yz);
            wc_pixel = hitx1.xyz;
            wc_normal = vec3(1.0, 0.0, 0.0);
                // The X=1 face has a normal pointing -X
            ambient = vec3(0.1, 0.0, 0.0);
            diffuse = vec3(1.0, 0.0, 0.0);
        } //endif hit X=0 else
    } //endif not parallel to X
    if(did_hit) return true;

    // Y faces
    if(abs(camera_dir.y)>CUBEHIT_THLD) {    //Don't shoot parallel to Y plane
        vec4 hity0 = HitYPlane(camera_pos, camera_dir, 0.0);
        vec4 hity1 = HitYPlane(camera_pos, camera_dir, 1.0);
        if(hity0.w<hity1.w) {   // check Y=0 hit
            did_hit = IsPointInRectXY(vec4(0,0,1,1), hity0.xz);
            wc_pixel = hity0.xyz;
            wc_normal = vec3(0.0, -1.0, 0.0);
            ambient = vec3(0.0, 0.1, 0.0);
            diffuse = vec3(0.0, 0.5, 0.0);
        } else {            // check Y=1 hit
            did_hit = IsPointInRectXY(vec4(0,0,1,1), hity1.xz);
            wc_pixel = hity1.xyz;
            wc_normal = vec3(0.0, 1.0, 0.0);
            ambient = vec3(0.0, 0.1, 0.0);
            diffuse = vec3(0.0, 1.0, 0.0);
        } //endif hit Y=0 else
    } //endif not parallel to Y
    if(did_hit) return true;

    // Z faces
    if(abs(camera_dir.z)>CUBEHIT_THLD) {    //Don't shoot parallel to Z plane
        vec4 hitz0 = HitZPlane(camera_pos, camera_dir, 0.0);
            //coords and t of Z=0 hit
        vec4 hitz1 = HitZPlane(camera_pos, camera_dir, 1.0);
            // NOTE: may get unpredictable values when looking down the +Z axis
        if(hitz0.w<hitz1.w) {   // check Z=0 hit
            did_hit = IsPointInRectXY(vec4(0,0,1,1), hitz0.xy);
            wc_pixel = hitz0.xyz;
            wc_normal = vec3(0.0, 0.0, -1.0);
            ambient = vec3(0.0, 0.0, 0.1);
            diffuse = vec3(0.0, 0.0, 0.5);
        } else {            // check Z=1 hit
            did_hit = IsPointInRectXY(vec4(0,0,1,1), hitz1.xy);
            wc_pixel = hitz1.xyz;
            wc_normal = vec3(0.0, 0.0, 1.0);
            ambient = vec3(0.0, 0.0, 0.1);
            diffuse = vec3(0.0, 0.0, 1.0);
        } //endif hit Z=0 else
    } //endif not parallel to Z
    if(did_hit) return true;

    return false;
} //cube_hit

// }}}1

// CHARSET AND WORLD-COORDINATE LETTERS ///////////////
// {{{1
/*
     00000
   1   2   3
   1   2   3
   1   2   3
     44444
   5       6
   5       6
  858      6
  88877777
  888
(lowercase x, dot, and bang are handled separately)
*/

// Character storage
#define NSEGS (9)
vec4 SEG_SHAPES[NSEGS];
    // All polys will be quads in the X-Y plane, Z=0.
    // All quad edges are parallel to the X or Y axis.
    // These quads are encoded in a vec4: (.x,.y) is the LL corner and
    // (.z,.w) is the UR corner (coords (x,y)).

// Grid parameters
#define GRID_CHARHT (10.0)
#define GRID_CHARWD (6.0)
    // Size of each character
#define GRID_PITCH (7.0)
    //each char takes up this much space.  Margin is added on the right.
#define GRID_PITCH_RECIP (0.14285714285714285714285714285714)
    // avoid a division
#define THICKNESS (1.0)
    // how thick each stroke is

#define GRID_VSQUISH (0.5)
    // How much letters are squished vertically after they are rotated,
    // i.e., along the X axis
#define GRID_VPITCH (6.0)
#define GRID_VPITCH_RECIP (0.16666666666666666666666666666667)
    // The pitch on the X axis _after_ CHARHT is multiplied by VSQUISH.

/*
    For upright chars, each char (X,Y) goes from (PITCH*ofs, 0)->(.+WD,HT).
    For sideways chars, (VPITCH*ofs, 0)->(.+HT, WD).
    Rotation is 90 deg CCW.
*/

void init_charset()
{
    float halft = THICKNESS*0.5;
    float halfht = GRID_CHARHT * 0.5;

    SEG_SHAPES[0] = vec4(THICKNESS, GRID_CHARHT - THICKNESS, GRID_CHARWD-THICKNESS, GRID_CHARHT);
    SEG_SHAPES[1] = vec4(0.0,                   halfht, THICKNESS,             GRID_CHARHT - halft);
    SEG_SHAPES[2] = vec4(GRID_CHARWD*0.5-halft, halfht, GRID_CHARWD*0.5+halft, GRID_CHARHT - halft);
    SEG_SHAPES[3] = vec4(GRID_CHARWD-THICKNESS, halfht, GRID_CHARWD,           GRID_CHARHT - halft);
    SEG_SHAPES[4] = vec4(THICKNESS, halfht - halft, GRID_CHARWD-THICKNESS, halfht + halft);
    SEG_SHAPES[5] = vec4(0.0,                   halft,    THICKNESS,             halfht );
    SEG_SHAPES[6] = vec4(GRID_CHARWD-THICKNESS, halft,    GRID_CHARWD,           halfht );
    SEG_SHAPES[7] = vec4(THICKNESS, 0, GRID_CHARWD-THICKNESS, THICKNESS);
    SEG_SHAPES[8] = vec4(0.0, 0.0, THICKNESS, THICKNESS); //dot
}

// Text-rendering internal parameters
#define LETTER_EPSILON (0.001)
    // small enough for our purposes.
#define SIDE_LETTERS (4)
    // How many letters to render on each side of the current one.
    // Set to fill the screen at the desired aspect ratio and orientation.

bool is_in_zzero_message(in vec2 world_xy_of_point,
                         in float middle_charidx, in float middle_x,
                         in float clip_charidx)
{   // returns true iff world_xy_of_point is in a letter
    // upright in the z=0 plane.
    // Letters are extracted from the message, with message[middle_charidx]
    // being displayed with its LL corner at (middle_x, 0, 0).
    // Characters starting from clip_charidx are not hit.

    // Check each letter in turn
    for(int ltr_idx=0; ltr_idx<(2*SIDE_LETTERS+1); ++ltr_idx) {
        float letter_delta = float(ltr_idx-SIDE_LETTERS-1);
        float thisletterindex = letter_delta + middle_charidx;
            // so the middle element of ltr_idx maps to middle_charidx

        if(thisletterindex >= clip_charidx) {
            break;  // no more letters
        }

        float mask = get_seg_mask(thisletterindex);
            // the segments for this letter

        // Early exit on spaces
        if(mask <= LETTER_EPSILON) {
            continue; //to next letter
        }

        // Where is this letter on the X axis?
        float ofs = (letter_delta*GRID_PITCH) + middle_x;

        // check each segment in turn
        for(int seg_idx=0; seg_idx<NSEGS; ++seg_idx) {
            if(mod(mask, 2.0)>LETTER_EPSILON) {
                // Where is this segment of this letter?
                vec4 theshape = SEG_SHAPES[seg_idx];
                theshape += vec4(ofs, 0.0, ofs, 0.0);
                    //shift it over to the right place

                // Check if we are in the segment
                if(IsPointInRectXY(theshape, world_xy_of_point)) {
                    return true;    // as soon as we're in a segment, we don't need to check any others
                }

            } //endif this segment is in mask

            mask = floor(mask * 0.5);
                //move to next bit and drop fractional part

            // Early exit when you run out of segments
            if(mask<=LETTER_EPSILON) {
                break; //to next letter
            }
        } //foreach segment

    } //foreach letter

    return false;
} //is_in_zzero_message

bool is_in_zzero_sine_message(in vec2 world_xy_of_point,
                         in float middle_charidx, in float middle_x,
                         in float clip_charidx,
                         in float cam_look_at_x)
{   // returns true iff world_xy_of_point is in a letter
    // upright in the z=0 plane, sinescrolled.
    // Letters are extracted from the message, with message[middle_charidx]
    // being displayed with its LL corner at (middle_x, 0, 0).
    // Characters starting from clip_charidx are not hit.

    // Check each letter in turn
    for(int ltr_idx=0; ltr_idx<(2*SIDE_LETTERS+1); ++ltr_idx) {
        float letter_delta = float(ltr_idx-SIDE_LETTERS-1);
        float thisletterindex = letter_delta + middle_charidx;
            // so the middle element of ltr_idx maps to middle_charidx

        if(thisletterindex >= clip_charidx) break;  // no more letters
        float mask = get_seg_mask(thisletterindex);
        if(mask <= LETTER_EPSILON) continue; //to next letter on space

        // Where is this letter?
        float xofs = (letter_delta*GRID_PITCH) + middle_x;
        float yofs = (0.5*sin(
                        TWO_PI*S_SINE_FREQ*xofs -
                        TWO_PI*S_SINE_GROUP_FREQ*cam_look_at_x
                     )) * GRID_CHARHT; // Each letter has its own phase

        // check each segment in turn
        for(int seg_idx=0; seg_idx<NSEGS; ++seg_idx) {
            if(mod(mask, 2.0)>LETTER_EPSILON) {
                // Where is this segment of this letter?
                vec4 theshape = SEG_SHAPES[seg_idx];
                theshape += vec4(xofs, yofs, xofs, yofs);
                    //shift it over to the right place
                // Check if we are in the segment
                if(IsPointInRectXY(theshape, world_xy_of_point)) {
                    return true;    // as soon as we're in a segment, we don't need to check any others
                }
            } //endif this segment is in mask

            mask = floor(mask * 0.5);   // Next segment

            if(mask<=LETTER_EPSILON) break; //to next letter if no more segs
        } //foreach segment
    } //foreach letter
    return false;
} //is_in_zzero_sine_message

bool is_in_sideways_message(in vec2 world_xy_of_point,
                            in float middle_charidx, in float middle_x,
                            in float clip_charidx)
{   // returns true iff world_xy_of_point is in a letter
    // sideways in the z=0 plane.
    // Letters are extracted from the message, with message[middle_charidx]
    // being displayed with its UL corner (i.e., the LL corner of its
    // bounding box) at (middle_x, 0, 0).

    // Check each letter in turn
    for(int ltr_idx=0; ltr_idx<(2*SIDE_LETTERS+1); ++ltr_idx) {
        float letter_delta = float(ltr_idx-SIDE_LETTERS-1);
        float thisletterindex = letter_delta + middle_charidx;
            // so the middle element of ltr_idx maps to base_letterindex

        if(thisletterindex >= clip_charidx) {
            break;  // no more letters
        }

        float mask = get_seg_mask(thisletterindex);
            // the segments for this letter

        // Early exit on spaces
        if(mask <= LETTER_EPSILON) {
            continue; //to next letter
        }

        // Determine the offset of the current letter
        float ofs = letter_delta*GRID_VPITCH + middle_x;
            // GRID_VPITCH*0.5;
            // + pitch/0.5 because letters were snapping into
            // existence at the right side of the screen.

        // check each segment in turn
        for(int seg_idx=0; seg_idx<NSEGS; ++seg_idx) {
            if(mod(mask, 2.0)>LETTER_EPSILON) {
                // Where is this segment of this letter, rotated 90 deg CCW?
                vec4 theshape = SEG_SHAPES[seg_idx].wxyz;
                theshape.xz = vec2(GRID_CHARHT) - theshape.xz;
                    // The effect of these two lines is to
                    // 1. swap X and Y components of both corners - CW rotation
                    // 2. swap ends so it's a CCW rotation
                    // 3. Rearrange so we have LL and UR corners rather than
                    // UL and LR corners.

                theshape *= vec4(GRID_VSQUISH, 1.0, GRID_VSQUISH, 1.0);
                    // Shrink the letters along X, i.e., vertically from the
                    // letter's point of view.
                theshape += vec4(ofs, 0.0, ofs, 0.0);
                    //shift it over to the right place

                // Check if we are in the segment
                if(IsPointInRectXY(theshape, world_xy_of_point)) {
                    return true;    // as soon as we're in a segment, we don't need to check any others
                }

            } //endif this segment is in mask

            mask = floor(mask * 0.5);
                //move to next bit and drop fractional part

            // Early exit when you run out of segments
            if(mask<=LETTER_EPSILON) {
                break; //to next letter
            }
        } //foreach segment

    } //foreach letter

    return false;
} //is_in_sideways_message

vec4 is_in_standard_zzero(in vec3 camera_pos, in vec3 rayend,
        in float charidx_frac, in float first_charidx,
        in float clip_charidx)
{   //Helper for the common check for straight ZZero text.
    //Returns .w==1.0 if hit, else 0.0.  If hit, .xyz are the hitlocn.

    vec3 hitlocn = HitZZero(camera_pos, rayend);
        // hitlocn is where it hits z=0, where the letters are

    // Determine the offset of the current letter
    float curr_charidx = floor(charidx_frac);
    float ofs = curr_charidx*GRID_PITCH + GRID_PITCH*0.5;
        // + pitch/0.5 because letters were snapping into
        // existence at the right side of the screen.

    if(is_in_zzero_message(hitlocn.xy,
                            curr_charidx+first_charidx,
                            ofs, clip_charidx)) {
        return vec4(hitlocn, 1.0);
    }

    return vec4(0.0);   //else return no-hit
} //is_in_standard_zzero

// }}}1

// ARTISTRY ///////////////////////////////////////////
// {{{1

#define COLORCYL_EPS (0.1)
#define COLORCYL_X_GRID_PITCH (50.0)

vec3 color_cylinder(in vec4 cyl, in float time)
{   //takes the result of cylinder_hit and returns a diffuse color.
    // Used by CYL and TUNNEL.
    vec3 diffuse;
    if(mod(cyl.x,COLORCYL_X_GRID_PITCH)<COLORCYL_EPS ||
       mod(cyl.a,PI*0.5)<0.01) {        //grid
        diffuse = vec3(0.0);
    } else {                            // colored background
        float hue_0_to_1 = (cyl.a+PI)*ONE_OVER_TWO_PI;
            // cyl.a is atan2 output [-PI,PI].  Move to [0,2PI], then scale to [0,1].
        float value_0_to_1 = smoothstep(0.0, 10.0, time)*0.5;
            // Fade in the cylinder over 10 sec.
        diffuse = hsv2rgb(vec3(hue_0_to_1,mod(cyl.x,1.0),value_0_to_1));
    }
    return diffuse;
} //color_cylinder

// }}}1

// CAMERA AND LIGHT ///////////////////////////////////
// {{{1

// --- Helpers ---

#define GAMMA (2.2)
#define ONE_OVER_GAMMA (0.45454545454545454545454545454545)

vec3 do_phong(in vec3 pixel_pos, in vec3 wc_normal, in vec3 camera_pos,
                in vec3 light_pos, in vec3 ambient,
                in vec3 diffuse, in float shininess)
{   // Compute Phong shading.  Modified from
    // https://en.wikipedia.org/wiki/Blinn%E2%80%93Phong_shading_model
    // wc_normal must be normalized on input.  All inputs are world coords.
    // Set shininess negative to turn off specular highlights.
    vec3 lightDir = normalize(light_pos - pixel_pos);
    float lambertian = max(dot(lightDir,wc_normal), 0.0);
    float specular = 0.0;

    if((lambertian > 0.0) && (shininess >= 0.0)) {
        vec3 viewDir = normalize(camera_pos-pixel_pos);
        vec3 reflectDir = reflect(-lightDir, wc_normal);
        float specAngle = max(dot(reflectDir, viewDir), 0.0);
        specular = pow(specAngle, shininess);
    }
    /*
    return pow(ambient + lambertian*diffuse + specular*vec3(1.0),
                vec3(ONE_OVER_GAMMA));
        // Do I need this?
    */
    return ambient + lambertian*diffuse + specular*vec3(1.0);

} //do_phong

highp vec3 pos_clelies(in float time, in float radius)
{   //Clelies curve
    //thanks to http://wiki.roblox.com/index.php?title=Parametric_equations
    vec3 pos; float m = 0.8;
    highp float smt = sin(m*time);
    pos.x = radius * smt*cos(time);
    pos.y = radius * smt*sin(time);
    pos.z = radius * cos(m*time);
    return pos;
} //camerapos

// --- Per-part routines ---

void do_cl_nop(in float partnum, in float charidx_frac, out vec3 camera_pos,
     out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg,
     out vec3 light_pos)
{
    camera_pos = vec3(0.0,0.0,10.0);    //default
    camera_look_at = vec3(0.0);
    camera_up = vec3(0.0, 1.0, 0.0);
    fovy_deg = 45.0;
    light_pos = camera_pos;
}

void do_cl_s_plain(in float partnum, in float charidx_frac, out vec3 camera_pos,
     out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg,
     out vec3 light_pos)
{
    camera_pos = vec3(charidx_frac*GRID_PITCH-5.0, GRID_CHARHT*0.5 + 1.5, 10.0);
    camera_look_at = vec3(camera_pos.x+3.0, GRID_CHARHT*0.5,0);
    camera_up = vec3(0.0, 1.0, 0.0);
    fovy_deg = 30.0;
    light_pos = camera_pos;
}

void do_cl_starwars(in float partnum, in float charidx_frac, out vec3 camera_pos,
     out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg,
     out vec3 light_pos)
{
    camera_pos = vec3(charidx_frac*GRID_VPITCH-5.0, GRID_CHARWD*0.5, 5.0);
    camera_look_at = vec3(camera_pos.x-10.0, camera_pos.y, 0.0);
    camera_up = vec3(-1.0,0.0,0.0);
    fovy_deg = 15.0;
    light_pos = camera_pos;
}

void do_cl_s_zoom(in float partnum, in float charidx_frac, out vec3 camera_pos,
     out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg,
     out vec3 light_pos)
{
    camera_pos = vec3(charidx_frac*GRID_PITCH, GRID_CHARHT*0.5,
                        5.0*sin(charidx_frac)+10.0); //z from 5 to 15
    camera_look_at = vec3(camera_pos.xy,0.0);
    camera_up = vec3(0.0, 1.0, 0.0);
    fovy_deg = 45.0;
    light_pos = vec3(camera_pos.x+GRID_PITCH, camera_pos.yz);
}

void do_cl_s_sine(in float partnum, in float charidx_frac, out vec3 camera_pos,
     out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg,
     out vec3 light_pos)
{
    camera_pos = vec3(charidx_frac*GRID_PITCH,
                        GRID_CHARHT*(0.5 /*+sin(charidx_frac)*/ ), 10.0);
    camera_look_at = vec3(camera_pos.xy,0.0);
    camera_up = vec3(0.0, 1.0, 0.0);
    fovy_deg = 45.0;
    light_pos = camera_pos;
}

void do_cl_s_roto(in float partnum, in float charidx_frac, out vec3 camera_pos,
     out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg,
     out vec3 light_pos)
{
    camera_pos = vec3(charidx_frac*GRID_PITCH, GRID_CHARHT*0.5, 10.0);
    camera_look_at = vec3(camera_pos.xy,0.0);
    //Rotation
    float c = cos(charidx_frac), s = sin(charidx_frac);
    mat2 rot_2d = mat2(vec2(c,s), vec2(-s,c));
    camera_up = vec3(rot_2d*vec2(0.0, 1.0),0.0);
    fovy_deg = 45.0;
    light_pos = camera_pos;
} //do_cl_s_roto

void do_cl_bold(in float partnum, in float charidx_frac, out vec3 camera_pos,
     out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg,
     out vec3 light_pos)
{
    camera_pos = vec3(1.6*GRID_PITCH+0.5*GRID_PITCH,0.7*GRID_CHARHT,10.0);
        // So I can use is_in_standard_zzero ^^^
    camera_look_at = vec3(camera_pos.x, 0.5*GRID_CHARHT, 0.0);
    camera_up = vec3(0.0, 1.0, 0.0);
    fovy_deg = 35.0;
    light_pos = camera_pos;
}

void do_cl_cyl(in float partnum, in float charidx_frac, out vec3 camera_pos,
     out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg,
     out vec3 light_pos)
{
    // DEBUG: for now, just use plain
    do_cl_s_plain(partnum, charidx_frac, camera_pos, camera_look_at,
                    camera_up, fovy_deg, light_pos);
    return;
    /*
    // DEBUG: Currently using this as a sphere tester
    camera_pos = vec3(0.0,0.0,10.0);    //default
    camera_look_at = vec3(0.0);
    camera_up = vec3(0.0, 1.0, 0.0);
    fovy_deg = 45.0;
    light_pos = pos_clelies(charidx_frac, 20.0);
    */
}

void do_cl_tunnel(in float partnum, in float charidx_frac, out vec3 camera_pos,
     out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg,
     out vec3 light_pos)
{
    vec3 cyl_cam_at, cyl_cam_look;

    float actual_time = charidx_frac + TUNNEL_START;
        // works because rate=1 for tunnel
    float synth_cyl_charidx_frac = (actual_time-CYL_START)*4.0;
        // copied from get_story.  Where we would be if CYL had continued.

    // Transition smoothly from cyl to tunnel.  First, get where we
    // would have been in CYL.
    do_cl_cyl(partnum, synth_cyl_charidx_frac, cyl_cam_at, cyl_cam_look,
                camera_up, fovy_deg, light_pos);

    float x_at_cyl_plus_2sec =
        (TUNNEL_START + 2.0 - CYL_START)*4.0*GRID_PITCH-5.0;
        // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^ relative time in CYL for TUNNEL+2sec
        //                  ^^^^^^^^^^^^^^^^ rate from get_story
        //                                  ^^^^^^^^^^^^^^^ x from cl_cyl

    vec3 tun_cam_at, tun_cam_look;

    tun_cam_at = vec3(cyl_cam_at.x /*x_at_cyl_plus_2sec*/, 0.0, 0.0);
        // Over 2 sec., transition to same X but yz=0.
    tun_cam_look = vec3(cyl_cam_at.x+8.0, 0.0, 0.0);  //straight down the X axis
        // rotate look_at based on current x

    float ratio = smoothstep(0.0, 2.0, charidx_frac);
        // move the camera over 2 sec. (since, for tunnel,
        // charidx_frac===time_in_part).
    vec3 camera_pos_transition = mix(cyl_cam_at, tun_cam_at, ratio);
    vec3 look_at_transition = mix(cyl_cam_look, tun_cam_look, ratio);

    // wobble the look_at after the transition
    float radius = smoothstep(2.0, 10.0, charidx_frac)*6.0;
    float delta_t = charidx_frac-2.0;
    vec3 wobble = vec3(0.0, radius*cos(delta_t), radius*sin(delta_t));
    //camera_look_at = look_at_transition + wobble;

    // Post-transition, also accelerate.
    float cyl_xspeed_at_2sec = 4.0*GRID_PITCH;   // rate 4 cps per get_story
    float newx = x_at_cyl_plus_2sec +
                    (cyl_xspeed_at_2sec*delta_t) +
                    (0.5*TUNNEL_ACCEL*delta_t*delta_t);
    vec3 camera_pos_accel = vec3(newx, 0.0, 0.0);
    vec3 look_at_accel = vec3(camera_pos_accel.x+6.0, wobble.yz);

    // Hard cutover between transition and accel at 2.0 sec.
    // They are set up to overlap, so it works OK.
    camera_pos =
        mix(camera_pos_transition, camera_pos_accel, step(0.0, delta_t));
    camera_look_at = mix(look_at_transition,
                            look_at_accel, step(0.0, delta_t));

} //do_cl_tunnel

void do_cl_copout(in float partnum, in float charidx_frac, out vec3 camera_pos,
     out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg,
     out vec3 light_pos)
{
    // DEBUG: for now, just use plain
    do_cl_s_plain(partnum, charidx_frac, camera_pos, camera_look_at,
                    camera_up, fovy_deg, light_pos);
    return;
    camera_pos = vec3(0.0,0.0,10.0);    //default
    camera_look_at = vec3(0.0);
    camera_up = vec3(0.0, 1.0, 0.0);
    fovy_deg = 45.0;
    light_pos = camera_pos;
}

void do_cl_twosided(in float partnum, in float charidx_frac, out vec3 camera_pos,
     out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg,
     out vec3 light_pos)
{
    float theta = sin(charidx_frac*TWOSIDED_RATE);
    camera_pos = vec3(charidx_frac*GRID_PITCH-20.0, GRID_CHARHT*0.5 + 1.5,
        theta*10.0);
    camera_look_at = vec3(camera_pos.x+3.0, GRID_CHARHT*0.5,0);
    camera_up = vec3(0.0, 1.0, 0.0);
    fovy_deg = 45.0;
    light_pos = camera_pos;
} //do_cl_twosided

void do_cl_s_plasma(in float partnum, in float charidx_frac, out vec3 camera_pos,
     out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg,
     out vec3 light_pos)
{
    camera_pos = vec3(charidx_frac*GRID_PITCH+5.0, GRID_CHARHT*0.5 + 1.5, 10.0);
    camera_look_at = vec3(camera_pos.x-3.0, GRID_CHARHT*0.5,0);
    camera_up = vec3(0.0, 1.0, 0.0);
    fovy_deg = 30.0;
    light_pos = camera_pos;
} //do_cl_s_plasma

void do_cl_s_credz(in float partnum, in float charidx_frac, out vec3 camera_pos,
     out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg,
     out vec3 light_pos)
{
    // DEBUG: for now, just use plain
    do_cl_s_plain(partnum, charidx_frac, camera_pos, camera_look_at,
                    camera_up, fovy_deg, light_pos);
    return;
    camera_pos = vec3(0.0,0.0,10.0);    //default
    camera_look_at = vec3(0.0);
    camera_up = vec3(0.0, 1.0, 0.0);
    fovy_deg = 45.0;
    light_pos = camera_pos;
}

void do_cl_cube(in float partnum, in float charidx_frac, out vec3 camera_pos,
     out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg,
     out vec3 light_pos)
{   // for CUBE, charidx_frac is actually time_in_part.
    camera_pos = pos_clelies(charidx_frac, 10.0);
    camera_pos.xyz += vec3(0.5, 0.5, 0.5);
        // Look at the center of a unit cube from (0,0,0) to (1,1,1)
    camera_look_at = vec3(0.5);
    camera_up = vec3(0.0, 1.0, 0.0);
    fovy_deg = 5.0;
    light_pos = pos_clelies(charidx_frac, 20.0);
} //do_cl_cube

// }}}1

// MAIN ///////////////////////////////////////////////
// {{{1

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    // Init
    float time = iGlobalTime;
        // TODO sometime fix the double image at TWOSIDED_START - 2.0 + 4.72 :)
    init_charset();

    // --- Story ---
    vec4 story = get_story(time);
    float partnum=story[0], charidx_frac=story[1];
    float first_charidx=story[2], clip_charidx=story[3];

    // --- Camera and light ---
    vec3 camera_pos, camera_look_at, camera_up, light_pos;
    float fovy_deg;

    float cl_charidx_frac = mix(time - CUBE_START, charidx_frac,
                                clamp(distance(partnum, CUBE), 0.0, 1.0)
                               );
        // i.e., if partnum == CUBE, distance=0, so fixed at time-CUBE_START.
        // otherwise, distance(...)>=1.0, so charidx_frac.
        // And no if/then :)

    do_camera_light(partnum, cl_charidx_frac,
        camera_pos, camera_look_at, camera_up, fovy_deg, light_pos);

    // Camera processing
    mat4 view, view_inv;
    lookat(camera_pos, camera_look_at, camera_up,
            view, view_inv);

    mat4 proj, proj_inv;
    gluPerspective(fovy_deg, iResolution.x/iResolution.y, 1.0, 10.0,
                    proj, proj_inv);

    mat4 viewport, viewport_inv;
    compute_viewport(0.0, 0.0, iResolution.x, iResolution.y,
                        viewport, viewport_inv);

    // --- Geometry ---

    vec3 rayend = WorldRayFromScreenPoint(fragCoord,
                                    view_inv, proj_inv, viewport_inv).xyz;
        // rayend-camera_pos is the direction of the ray

    // Each part determines world coords of the hit, normal at the
    // hit point, and base color of the geometry.

    vec3 wc_pixel;  // world coords of this pixel
    vec3 wc_normal; // ditto for the normal
    vec3 ambient = vec3(0.0);
    vec3 diffuse = vec3(0.0);
        // material - light is always white.  Alpha is always 1.
    float shininess = 4.0;  //Phong shininess
    bool did_hit = false;   //if not did_hit, just use _diffuse_.

    if((partnum != NOP) && (partnum != STARWARS) &&
        (partnum != S_SINE) && (partnum != BOLD) &&
        (partnum != CYL) && (partnum != TUNNEL) &&
        (partnum != CUBE)) {        // Basic scrollers

        vec4 hit_data = is_in_standard_zzero(camera_pos, rayend,
                            charidx_frac, first_charidx, clip_charidx);
        if(hit_data.w>0.0) {
            did_hit = true;
            wc_pixel = hit_data.xyz;
            wc_normal = vec3(0.0,0.0,-1.0 + 2.0*step(0.0, camera_pos.z));
                // normal Z is -1 if camera_pos.z<0.0, and +1 otherwise.
                // This benefits TWOSIDED.
            ambient = vec3(0.2, 0.2, 0.1);
            diffuse = vec3(0.6,0.6,0.3);
            shininess = 20.0;
        }  // else diffuse is the default (0,0,0).
    } else //endif S_PLAIN and others

    if(partnum == STARWARS) {   // see is_in_standard_zzero for explanation
        vec3 hitlocn = HitZZero(camera_pos, rayend);
        float curr_charidx = floor(charidx_frac);
        float ofs = curr_charidx*GRID_VPITCH;

        if(is_in_sideways_message(hitlocn.xy,
                                    curr_charidx+first_charidx,
                                    ofs, clip_charidx)) {
            did_hit = true;
            wc_pixel = hitlocn;
            wc_normal = vec3(0.0,0.0,1.0);
            ambient = vec3(0.0, 0.2, 0.2);
            diffuse = vec3(0.0,1.0,1.0);
            shininess = 1.0;
        }
    } else //endif STARWARS

    if(partnum == S_SINE) {
        vec3 hitlocn = HitZZero(camera_pos, rayend);
        float curr_charidx = floor(charidx_frac);
        float ofs = curr_charidx*GRID_PITCH + GRID_PITCH*0.5;

        if(is_in_zzero_sine_message(hitlocn.xy,
                                curr_charidx+first_charidx,
                                ofs, clip_charidx,
                                camera_look_at.x)) {
            did_hit = true;
            wc_pixel = hitlocn;
            wc_normal = vec3(0.0,0.0,1.0);
            ambient = vec3(0.1, 0.2, 0.1);
            diffuse = vec3(0.3,0.9,0.3);
            shininess = 20.0;
        }  // else diffuse is the default (0,0,0).
    } else //endif S_SINE

    if(partnum == BOLD) {
        vec4 hit_data = is_in_standard_zzero(camera_pos, rayend,
                            3.0, first_charidx, clip_charidx);
        if(hit_data.w>0.0) {
            did_hit = true;
            wc_pixel = hit_data.xyz;
            wc_normal = vec3(0.0,0.0,1.0);
            shininess = -1.0;   //just the texture
            ambient = vec3(0.0);

            // Do some palette animation
            vec2 tex_coord;
            tex_coord.s = hit_data.x / ((clip_charidx-first_charidx)*GRID_PITCH);
            tex_coord.t = hit_data.y / GRID_CHARHT;
            vec4 tex = texture2D(iChannel1, tex_coord);
            float hue = mod(time+tex.b, 1.0);
            diffuse = hsv2rgb(vec3(hue,0.7,1.0));

            float howmuch = smoothstep(3.0, 5.0, time-BOLD_START);
            diffuse = mix(diffuse, vec3(0.0), howmuch);
        } //endif did hit
    } else //endif BOLD

    if(partnum == CYL) {
        /*  // a sphere, for debug
        wc_pixel = sphere_hit(camera_pos, rayend, vec3(0.0), 4.0);
        if(length(wc_pixel)>0.0) {
            did_hit = true;
            wc_normal = normalize(wc_pixel);    //centered at the origin
            ambient = vec3(0.3, 0.0, 0.3);
            diffuse = vec3(0.3,0.0,0.3);
        }
        */
        shininess = 20.0;
        vec4 hit_data = is_in_standard_zzero(camera_pos, rayend,
                            charidx_frac, first_charidx, clip_charidx);
        if(hit_data.w>0.0) {
            did_hit = true;
            wc_pixel = hit_data.xyz;
            wc_normal = vec3(0.0,0.0,1.0);
            ambient = vec3(0.1, 0.2, 0.2);
            diffuse = vec3(0.3,0.6,0.6);
        } else {    //Test it against the cylinder
            ambient = vec3(0.1);
            vec4 cyl = cylinder_hit(CYL_RADIUS, camera_pos, rayend);
                // xyz,\theta
            wc_normal = normalize(vec3(0.0, -cyl.yz));
                // Normal is always straight inward, so no X component.
                // Cylinder is centered around axis, so negate y and z.

            diffuse = color_cylinder(cyl, time-CYL_START);
        } //endif text else cylinder
    } else //endif CYL

    if(partnum==TUNNEL) {
        shininess = 20.0;
        ambient = vec3(0.1);
        // TODO fog out the center
        vec4 cyl = cylinder_hit(CYL_RADIUS, camera_pos, rayend);
            // xyz,\theta
        wc_normal = normalize(vec3(0.0, -cyl.yz));
            // Normal is always straight inward, so no X component.
            // Cylinder is centered around axis, so negate y and z.

        vec3 cyldiffuse = color_cylinder(cyl, time-CYL_START);
            // CYL_START, not TUNNEL_START, for continuity
        float ratio = smoothstep(S_SINE_START-2.0, S_SINE_START, time);
            //over the last 2.0 seconds of TUNNEL, before S_SINE...
        diffuse = mix(cyldiffuse, vec3(0.0), ratio);
            //... fade out the cylinder

    } else //endif TUNNEL

    if(partnum==CUBE) {     // End scene - cube and logo
        shininess = 10.0;

        // Check the logo first.  7 letters - fixed

        // The logo has its own camera.
        // Note that the lighting of the logo will still use
        // the cube's camera_pos.

        vec3 logo_camera_pos = vec3(7.0*0.5*GRID_PITCH,
                        GRID_CHARHT * 0.2 /* GRID_CHARHT*0.5 */, 10.0);
        mat4 logo_view, logo_view_inv;
        lookat( logo_camera_pos,
                vec3(logo_camera_pos.x, GRID_CHARHT * 0.6, 0.0),  //look_at
                vec3(0.0,1.0,0.0),              //camera_up
                logo_view, logo_view_inv);

        mat4 logo_proj, logo_proj_inv;
        gluPerspective(55.0, iResolution.x/iResolution.y, 1.0, 10.0,
                        logo_proj, logo_proj_inv);

        // Don't recompute viewport - it's the same
        vec3 logo_rayend = WorldRayFromScreenPoint(fragCoord,
                        logo_view_inv, logo_proj_inv, viewport_inv).xyz;
        // rayend-camera_pos is the direction of the ray

        vec3 logo_hitlocn = HitZZero(logo_camera_pos, logo_rayend);
        // hitlocn is where it hits z=0, where the letters are

        // Determine the offset of the current letter
        float logo_curr_charidx = 3.0;   //always the middle one
        float logo_ofs = logo_curr_charidx*GRID_PITCH;

        if(is_in_zzero_message(logo_hitlocn.xy,
                                logo_curr_charidx+first_charidx,
                                logo_ofs, clip_charidx)) {
            did_hit = true;
            wc_pixel = logo_hitlocn;
            wc_normal = vec3(0.0,0.0,1.0);
            ambient = vec3(0.3, 0.3, 0.1);
            diffuse = vec3(1.0,1.0,0.0);
            shininess = 10.0;

        } else {    //Check for a hit on the cube
            vec3 tempdiffuse;   // since diffuse always gets trashed
            did_hit = cube_hit(camera_pos, rayend, wc_pixel, wc_normal,
                                    ambient, tempdiffuse);
            if(did_hit) {
                diffuse = tempdiffuse;
            }

        } //endif didn't hit logo else

    } //endif CUBE

    // --- Lighting ---
    // Phong shading based on the Geometry section's output values

    if(did_hit) {               // a hit
        fragColor = vec4(
            do_phong(wc_pixel, wc_normal, camera_pos, light_pos,
                        ambient, diffuse, shininess),
            1.0);
        if(partnum == CUBE) {
            // fade in the colors
            float fade = smoothstep(0.0, CUBE_FADEIN_TIME, time-CUBE_START);
            fragColor = mix(vec4(0.0,0.0,0.0,1.0), fragColor, fade);
        } //endif cube

    } else {                    // no hit - just use diffuse
        fragColor = vec4(diffuse, 1.0);
    }

} //mainImage

// }}}1

// vi: set ts=4 sts=4 sw=4 et ai foldmethod=marker foldenable foldlevel=0: //

