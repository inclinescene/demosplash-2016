#!/usr/bin/python3
# gen-music.py: for #08

# Note definitions: name and frequency (Hz)

# Notes, in six sharps - F# maj (F#, G#, A#, B, C#, D#, F (=E#))
# Frequencies in Hz, equal-tempered, A440.
# From http://www.phy.mtu.edu/~suits/notefreqs.html
R=('REST',0.0)
C4=('C4',277.18)
    # actually C#4, and likewise throughout.
D4=('D4',311.13)
E4=('E4',349.23)
F4=('F4',369.99)
G4=('G4',415.30)
A4=('A4',466.16)
B4=('B4',493.88)
C5=('C5',554.37)
D5=('D5',622.25)
E5=('E5',698.46)
F5=('F5',739.99)
G5=('G5',830.61)
A5=('A5',932.33)
B5=('B5',987.77)
C6=('C6',1108.73)
D6=('D6',1244.51)
notes=(C4, D4, E4, F4, G4, A4, B4, C5, D5, E5, F5, G5, A5, B5, C6, D6);

# music: list of ("note", duration in beats).
H=0.5;      # save typing - Half-beat.
music=[
    # bar 1
    (R,2),
    (A4,H),
    (B4,H),
    (C5,1.0),

    # bar 2
    (C5,1.0),
    (F5,1),
    (E5,1),
    (F5,1),

    # bar 3
    (G5,1),
    (A5,H),
    (F5,5.5),
    #(F6,10.0)  #sustained

    # bar 4
    (E5,H),
    (F5,H),

    #bar 5
    (C6,1.5),
    (F5,H),
    (F5,5.0),

    #bar 6
    (C5,H),   # tweaked to break up the run
    (F5,H),

    #bar 7
    (F5,2),
    (R,H),
    (G5,H),
    (F5,H),
    (E5,4.5),

    # bar 8 (p. 2)
    (A4,2),
    (R,H),
    (G4,H),
    (F4,H),
    (C5,1.5),

    # bar 9
    (R,1),
    (F5,1),
    (G5,1),

    #bar 10
    (B5,1.5),
    (A5,H),
    (A5,4),

    #bar 11
    (C6,1),
    (B5,1),
    
    #bar 12
    (A5,H),
    (B5,H),
    (A5,H),
    (F5,1),
    (C5,2),

    #bar 13
    (G5,H),
    (F5,H),
    (E5,H),
    (D5,H),
    (E5,H),
    (C5,H), # tweaked
    (R,H),  # tweaked

    #bar 14
    (F5,1.3),   # tweaked - a bit staccato
    (F5,1.3),   # ditto
    (F5,1)

    # the next bar is the one that has the glissando

    ]

###################################
# Emit the frequencies and periods

print("""// Tuning
#define F_REST (0.0)
    // for consistency
#define P_REST (0.0)""")

for (name, freq_hz) in notes:
    print("""\
#define F_%s (%.3f)
#define P_%s (%.20f)"""%(name, freq_hz, name, 1.0/freq_hz))

print()


###################################

# Preprocess music
music.append((R,0))     # always a rest at the end

# Print function header
print("""// Score
vec4 get_song_data(in float beat_in_pattern)
{
    //DuckTales Moon theme.
    //Music by Hiroshige Tonomura.  Transcribed by Manuel Gutierrez Rojas.
    //tweaked by cxw.
    vec4 retval; //(frequency, period (1/freq), startbeat, endbeat)
        // frequency < 1.0 => no sound
    retval = vec4(0.0);     // by default, no sound""")

# Print song data
currbeat = 0.0
for (note, duration) in music:
    (notename, notepitch) = note
    thisnote_vec4 = "vec4(F_%s, P_%s, %.3f, %.3f)"%(
                        notename, notename, currbeat, currbeat+duration)
    print("    retval = mix(retval, %s, step(%.3f, beat_in_pattern));"%
        (thisnote_vec4, currbeat))
    currbeat += duration

# Print function footer
print("""\
    return retval;
}""")

# vi: set ts=4 sts=4 sw=4 et ai: #

