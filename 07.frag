precision highp int;precision highp float;

// Parameters for your demo

// Parts and timing
#define BLACK_ID (0)
    // black screen - always starts at 0

#define COLORS_ID (1)
    // part number.  Use a float if you want to pack it in a vector.
#define COLORS_START (1.0)
    // seconds
#define MOIRE_ID (2)
#define MOIRE_START (COLORS_START + 5.0)

#define DEMO_DURATION_SEC (15.0)
    // How long a whole loop is
#define DEAD_TIME (1.0)
    // duration of black screen at the end of each part

// blinds
#define BLIND_CYCLE_TIME (MOIRE_START - COLORS_START)
#define BLIND_RAMP_TIME (1.0)
#define NBLINDS (7)
#define BLIND_EDGE_STEP (0.14285714285714285714285714285714)

// moire
#define THICKNESS (0.65)
#define SPACING (200.0)
#define SPEED (0.6)
#define FADEOUT_TIME_SEC (2.0)

// Computed parameters //////////////////////////////////////////////////
#define BLIND_END_RAMPDOWN_TIME (BLIND_CYCLE_TIME - DEAD_TIME)
#define BLIND_RAMP_RATE (1.0/BLIND_RAMP_TIME)
#define MOIRE_END_TIME (DEMO_DURATION_SEC - MOIRE_START - DEAD_TIME)

// Library routines and functions from earlier //////////////////////////

vec3 hsv2rgb(vec3 c) {
    // All inputs range from 0 to 1.  By hughsk, from
    // https://github.com/hughsk/glsl-hsv2rgb/blob/master/index.glsl .
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
} //hsv2rgb

/*from #04*/ float get_base_y(in float time, in float A, in float b, in float m, in float omega, in float phi) { return abs( A*exp(-b*time/(2.0*m))*cos(omega*time-phi) ); }
/*from #06*/ float get_sine_01( in float time, in vec2 coords, in vec2 center, in float radius, in float speed) { float whereami = radius*distance(center,coords) - speed*time; return 0.5+0.5*sin(whereami); }
/*from #06*/ float squish(in float f) { return (f+1.0)*0.5; }
/*from #06*/ vec2 lisa(in float time, in float a, in float b, in float delta) { return vec2(squish(sin(a*time+delta)), squish (sin(b*time))); }

float get_blind_mask(in float percent, in float x_01) {
    if(percent >= 1.0) { return 1.0; }
        // because at some resolutions, some pixels were failing **A**, below,
        // leaving black lines.  ** See debug notes 1 **
    float edges[NBLINDS+1];
    for(int edge_idx = 0; edge_idx < NBLINDS+1; ++edge_idx) {
        float edge_idx_f = float(edge_idx);   //explicit typecast
        edges[edge_idx] = smoothstep(0.0, 1.0, edge_idx_f * BLIND_EDGE_STEP);
    } //for each edge
    for(int blind_idx = 0; blind_idx < NBLINDS; ++blind_idx) {
        float left_edge = edges[blind_idx]; float right_edge = edges[blind_idx+1]; float center = mix(left_edge, right_edge, 0.5); float open_left = mix(center, left_edge, percent); float open_right = mix(center, right_edge, percent);
        if( (x_01 >= open_left) && (x_01 <= open_right) ) { // **A**
            return 1.0;     // This pixel is visible!
        }
    } //for each blind
    return 0.0;     // If we get here, the pixel is hidden by the blinds.
} //get_blind_mask

// Effect: colors ///////////////////////////////////////////////////////

vec4 do_colors(in float time_in_part, in vec2 pixel_coord_01)
{ // NOT global time, so we can move the effect around
    vec4 retval = vec4(     // A nice color stripe pattern
        hsv2rgb(vec3(pixel_coord_01.x+pixel_coord_01.y + time_in_part,
                        1.0,1.0)),   1.0);

    // Venetian blinds in, then out, on this.  As in #06.
    float blind_time = mod(time_in_part, BLIND_CYCLE_TIME);
    float blind_rise = blind_time * BLIND_RAMP_RATE;
    float blind_fall = (BLIND_END_RAMPDOWN_TIME - blind_time) * BLIND_RAMP_RATE;
    float blind_percent = clamp(min(blind_rise, blind_fall), 0.0, 1.0);
    float blind_mask = get_blind_mask(blind_percent, pixel_coord_01.x);
    retval = mix(vec4(0.0), retval, blind_mask);
    return retval;
} //do_colors

// Effect: moire ////////////////////////////////////////////////////////

vec4 do_moire(in float time, in vec2 pixel_coord_01 )
{ //used to be mainImage() - now it's moire() and has a "time" parameter.
    // Bounce it in - modify pixel_coord_01.  As in #04.
    float base_y = get_base_y(time, 1.0, 3.0, 1.0, 4.5, 0.0);
    vec4 retval = vec4(0.0);    //black by default

    if(pixel_coord_01.y >= base_y) {
        pixel_coord_01.y -= base_y;     // Shift the base

        // Code from last time
        vec2 ca=lisa(time*0.5*SPEED,5.0,4.0,0.0); vec2 cb=lisa(time*0.835744*SPEED,3.0,2.0,1.8); cb=mix(ca, cb, 0.6); float sa=get_sine_01(time, pixel_coord_01, ca, SPACING, 10.0); float sb=get_sine_01(time, pixel_coord_01, cb, SPACING, 10.0); sa=step(THICKNESS,sa); sb=step(THICKNESS,sb);
        //retval = vec4(max(sa,sb),0.0,0.0,1.0);
        retval = vec4(sa,0.0,sb*(1.0-step(0.1,sa)),1.0);
        // sb unless sa>=0.1 ^^^^^^^^^^^^^^^^^^^^^

        // Fade out at the end
        float fade_amount =
            clamp( (MOIRE_END_TIME - time) / FADEOUT_TIME_SEC, 0.0, 1.0);
        retval = mix(vec4(0.0), retval, fade_amount);
    }
    return retval;
} //do_moire

// mainImage() //////////////////////////////////////////////////////////

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    float t = mod(iGlobalTime, DEMO_DURATION_SEC);
        // Now you know one reason _t_ exists!
    vec2 pixel_coord_01 = fragCoord.xy / iResolution.xy;

    // Which part are we in?
    int part_id;
    float time_in_part;

    if(t < COLORS_START) {      //black screen
        part_id = BLACK_ID;
    } else if(t < MOIRE_START) {  //colors
        part_id = COLORS_ID;
        time_in_part = t - COLORS_START;
    } else {                    //moire
        part_id = MOIRE_ID;
        time_in_part = t - MOIRE_START;
    }

    // Run that part.  Separate if() block since some parts may share code.
    if(part_id == BLACK_ID) {                   //black screen
        fragColor = vec4(0.0,0.0,0.0,1.0);
    } else if(part_id == COLORS_ID) {           //colors
        fragColor = do_colors(time_in_part, pixel_coord_01);
    } else {                                    //moire
        fragColor = do_moire(time_in_part, pixel_coord_01);
    }
} //mainImage

// vi: set ts=4 sts=4 sw=4 et ai: //

