precision highp int;precision highp float;

vec4 get_sine_color(in float time, in vec2 coords)  // a slightly better name
{
    float whereami = 50.0*distance(vec2(0.5),coords) - 10.0*time;
    return vec4(0.0,0.0, 0.5+0.5*sin(whereami), 1.0);
} //get_sine_color

float get_base_y(in float time,
    in float A, in float b, in float m, in float omega, in float phi)
{ // determine where the bottom of the screen should be
    return abs( A*exp(-b*time/(2.0*m))*cos(omega*time-phi) );
        // Underdamped oscillator.  Take the absolute value so it
        // bounces rather than ringing around zero.
        // Use cos so that t=0 => base_y=1
        // Note: in production code, precompute K=-b/(2*m) and use exp(K*t)
} //get_base_y

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    float t = iGlobalTime;
    vec2 pixel_coord = fragCoord.xy / iResolution.xy;
    float base_y = get_base_y(t, 1.0, 3.0, 1.0, 4.5, 0.0);
        // Tweak empirically     ^^^^^^^^^^^^^^^^^^^^^^^
        // Note: don't pass parameters you don't need!  I didn't use
        // A, m, or phi.  Comment out so I can add it back later if I need it.

    if(pixel_coord.y < base_y) {
        fragColor = vec4(0.0);  //black at the bottom
    } else {
        pixel_coord.y -= base_y;
            // Move the generated image - pretend this pixel is where it
            // would be in the non-offset image.
        vec4 pixel_color = get_sine_color(t, pixel_coord);
        fragColor = pixel_color;
    }
} //mainImage

// parms for get_base_y are from 
// https://ocw.mit.edu/courses/mathematics/18-03sc-differential-equations-fall-2011/unit-ii-second-order-constant-coefficient-linear-equations/damped-harmonic-oscillators/MIT18_03SCF11_s13_2text.pdf
// Note: Keep b < sqrt(4*m*k) for a bounce (underdamping), where
// omega = sqrt(abs(b*b-4*m*k))/(2*m) (hint: MATLAB/GNU Octave)

// vi: set ts=4 sts=4 sw=4 et ai: //

