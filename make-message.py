#!/usr/bin/python3
# make-message.py: Print the message for ncl00.  cxw/incline
#message="HELLO WORLD 0123456789 abcdefghijklmnopqrstuvwxyz *)!@#$%^&*"

# message_human is one line per part, each line being <description>/<timeval>/<text>
# <description> should be suitable to be uppercased to be a constant.
rate = 4    # chars per sec.
# if <timeval>=0, nchars/rate is used.
# if <timeval> < 0, -timeval chars/sec. is the rate.
# Part 0 is a no-op for 1 sec., same as the audio dead space.
# In the text, '@' is a space that also marks the end of text to be displayed.
# Nothing at or after the @ is shown in that part.

# Parts starting with "S" are scrollers
message_human = """\
Nop/1/
S plain/0/     HELLO WORLD!  We are InclIne - the newest and most Improved demogroup ever!@@@@@@@@@
Starwars/-2.71828/InclIne |rockIng the rebel allIance sInce 2016    Emod was jarIg!     @
S Zoom/0/      We greet annI atparty bRs cmucc cvgm deUs-you can! gargaj-4 IncentIve! lumInescence@@@@@@@@
s Roto/0/     mandarIne monkey necta scenesat sIlentK vI yerzmyey and@@@@
Bold/6/YOU!@@@@@@@@@@
Cyl/0/  Let's rez up more geometry.@@@@@@@
Tunnel/10/
s Sine/0/       We are here to make somethIng awesome but thIs Is not It!  Stay tuned!@@@@@
Twosided/0/All polys raycast by hand In the USA from fRee-Range bIts never kept In quantum confInement!@@@@
S plasma/0/    Show us that fIrstprodvotIng stIll works In 2016!  ncl00- code cxw beats makro support phrank@@@@@
Cube/1000/InclIne"""

#------------------------------------------
# Font

def m(*segs):
    """ make a binary mask from the segments in the input iterable """
    rv=0
    for s in segs:
        rv += 2**s
    return rv

#     00000
#   1   2   3
#   1   2   3
#   1   2   3
#     44444
#   5       6
#   5       6
#  858      6
#  88877777
#  888

CHARS={
    # Single segments, for debugging.  Bang is handled separately.
    #')':1, '!':2, '@':4, '#':8, '$':16, '%':32, '^':64, '&':128,
    # Combined, also for debugging
    '*':255,
    # Punctuation
    ' ':0,
    '.':m(8),
    '!':m(1,8),
    '?':m(0,3,4,8),
    '-':m(4),
    '(':m(0,1,5,7),
    ')':m(0,3,6,7),
    '\'':m(2),
    '|':m(2),
    '_':m(7),
    '@':0,  # express blank, plus marks the clip char.
    # Alphanumerics
    '0':m(0,1,3,5,6,7),
    '1':m(3,6),
    '2':m(0,3,4,5,7),
    '3':m(0,3,4,6,7),
    '4':m(1,3,4,6),
    '5':m(0,1,4,6,7),
    '6':m(0,1,4,5,6,7),
    '7':m(0,3,6),
    '8':m(0,1,3,4,5,6,7),
    '9':m(0,1,3,4,6),
    # alpha
    'A':m(5,1,0,3,6,4),
    'a':m(5,1,0,3,6,4),
    'B':m(1,5,7,6,4),
    'b':m(1,5,7,6,4),
    'C':m(0,1,5,7),
    'c':m(0,1,4),
    'D':m(3,4,5,6,7),
    'd':m(4,5,7,6,3),
    'E':m(0,1,4,5,7),
    'e':m(4,3,0,1,5,7),
    'F':m(0,1,4,5),
    'f':m(0,1,4,5),
    'G':m(4,1,0,3,6,7),
    'g':m(4,1,0,3,6,7),
    'H':m(1,3,4,5,6),
    'h':m(1,4,5,6),
    'I':m(0,2,4),
    'i':m(1),   # TODO 3?  3,6?
    'J':m(3,6,7),
    'j':m(3,6,7),
    'K':m(1,2,4,5,6),
    'k':m(1,2,4,5,6),
    'L':m(1,5,7),
    'l':m(1,4), # TODO 3,6?  1,5,7?
    'M':m(0,1,2,3),
    'm':m(0,1,2,3),
    'N':m(0,1,3,5,6),
    'n':m(0,1,3),
    'O':m(0,1,5,7,6,3),
    'o':m(0,1,3,4),
    'P':m(0,1,3,4,5),
    'p':m(0,1,3,4,5),
    'Q':m(0,1,3,4,6),
    'q':m(0,1,3,4,6),
    'R':m(0,1,5),
    'r':m(0,1),
    'S':m(0,1,4,6,7),
    's':m(0,1,4,6,7),
    'T':m(1,4,5,7),
    't':m(1,4,5,7),
    'U':m(1,5,7,6,3),
    'u':m(1,3,4),       # let context distinguish it from 'v'
    'V':m(1,4,3),       # TODO special-case?
    'v':m(1,4,3),
    'W':m(1,2,3,4),
    'w':m(1,2,3,4),
    'X':m(2,3,4,5,6),   # TODO special-case it.
    'x':m(2,3,4,5,6),
    'Y':m(1,3,4,6,7),
    'y':m(1,3,4,6,7),
    'Z':m(0,3,4,5,7),
    'z':m(0,3,4,5,7),
}   # CHARS

############################################################################
import pdb
import re
from textwrap import indent, dedent

# Helpers

def id4(s, level=1):
    """ print _s_ with a four-space indent """
    return indent(dedent(s), prefix=(level*4)*' ')
#id4()

def print_fullmessage_bst(msg, st, en, level=1):
    """ Print an if/then/else BST of msg.  st is the first index to print
    and en is the last index + 1.  This makes a _huge_ difference to speed
    compared to a linear lookup over fullmessage."""
    THLD = 4    # below this, do it linear
    # Body
    if (en-st) > THLD:  # Recursive case
        mid = st + (en-st)//2   # integer divide
        print(id4("if(charidx>=%.1f){"%mid, level))
        print_fullmessage_bst(msg, mid, en, level+1)
        print(id4("}else{",level))
        print_fullmessage_bst(msg, st, mid, level+1)
        print(id4("}",level))

    else:               # base case
        for idx in reversed(range(st, en)):
            ch = msg[idx]

            if not (ch in CHARS):
                raise KeyError("Char %s @ %d/%d not in CHARS"%(ch,idx,len(msg)))
            else:
                # Don't output repeated chars
                if not ( (idx>0) and (idx>st) and (msg[idx-1]==ch) ):
                    #print(id4("""\
                    #if(charidx>=%.1f) { return %.1f; } // %s"""%(idx,
                    #                                    CHARS[ch],ch), level))
                    print(id4("""\
                    if(charidx>=%.1f) return %.1f;"""%(idx, CHARS[ch]), level))
    #endif recurse else
# print_fullmessage_bst()

##########################################################################
# Main

#------------------------------------------
# Process the message

NONPRINT = re.compile(r'[^a-z0-9_]', re.IGNORECASE)

message=[x.split('/') for x in message_human.split('\n')]

## Pad each row with some spaces so they won't overlap with the previous part?
#for idx, x in enumerate(message):
#    x[2] = 4*' ' + x[2]
#    message[idx] = x
##next x

# Parse out the pieces of the script
times=[float(x[1]) for x in message]
partnames=[]
starttimes=[0.0]
startcharidxes=[]
fullmessage=""
charcounts=[len(x[2]) for x in message]
rate_cps=[] # Chars per second in each part
clipcharidxes=[]

# Regularize the times and compute other useful things
for idx,t in enumerate(times):
    if t==0:    # use the default rate
        times[idx]=charcounts[idx]/rate    # float div
        rate_cps += [rate]
    elif t<0:   # negative of the rate was provided
        times[idx]=charcounts[idx]/(-t)
        rate_cps += [-t]
    else:       # time in seconds - compute the rate from the time
        rate_cps += [charcounts[idx]/times[idx]]    # float div

    if idx>0:
        starttimes += [sum(times[:idx])]

    startcharidxes += [len(fullmessage)]
    fullmessage += message[idx][2]
    partnames += [NONPRINT.sub('',message[idx][0].replace(' ','_')).upper()]

    # Clip char: no text shown from that point on.
    if '@' in message[idx][2]:  # Explicit: the first '@' in the text
        clipcharidxes += [startcharidxes[idx]+message[idx][2].find('@')]
    else:                       # Implicit: the off-the-end char of the
        clipcharidxes += [len(fullmessage)]     # part's text.
    #endif

# next idx,t

# The end times are shifted 1 from the start times
endtimes = starttimes[1:]
endtimes += [sum(times)]

# Story -----
# Part numbers and start times
print("// Parts and start times")
for idx, name in enumerate(partnames):
    print("#define %s (%.1f)"%(name,idx))
    print("#define %s_START (%.20f)"%(name,starttimes[idx]))
#next

# Header
print(dedent("""
    vec4 get_story(in float time)
    {   //returns vec4(partnum, charidx_frac, first_charidx, clip_charidx)
        // NOTE: charidx_frac restarts at 0 each part!
        // Character indices starting with clip_charidx should not be displayed.
        float partnum, charidx_frac, first_charidx, clip_charidx;"""
    ))

# Each part
# TODO: always print 0 as 0.0.  For now, use %.20f as a workaround.
for idx, tend in enumerate(endtimes):
    print(id4("""\
    if(time<%.20f) {
        partnum=%s;
        charidx_frac=(time-%s_START)*%.20f;
        first_charidx=%.1f;
        clip_charidx=%.1f;
    } else
    """%(tend, partnames[idx], partnames[idx],
        rate_cps[idx] if rate_cps[idx]>0 else 1,
        startcharidxes[idx], clipcharidxes[idx])))
#next idx,tstart

# Footer
print(id4("""\
    {
        partnum=0.0;
        charidx_frac=0.0;
        first_charidx=0.0;
        clip_charidx=0.0;
    }
    """))
print(dedent("""\
        return vec4(partnum,charidx_frac,first_charidx,clip_charidx);
    } //get_story
    """
    ))


# Text -----
# Header
print(dedent("""\
    float get_seg_mask(float charidx)
    {
        if(charidx>=%.1f) return 0.0; //blank at the end"""%len(fullmessage)))

# Body as a binary search tree
print_fullmessage_bst(fullmessage, 0, len(fullmessage))

# Footer
print(dedent("""\
        return 0.0; //empty by default
    } //get_seg_mask

    // Camera and light prototypes
    """))   #           ^^^ in the next block

# Light and camera dispatcher -----
LC_HDR = "void do_cl_%s(in float partnum, in float charidx_frac, \
out vec3 camera_pos, \
out vec3 camera_look_at, out vec3 camera_up, \
out float fovy_deg, out vec3 light_pos);"

# Generate prototypes
for n in partnames:
    print(LC_HDR%n.lower())

# Dispatcher.  Use one binary split for a touch of speed
mid = len(partnames)//2
print(dedent("""
    void do_camera_light(in float partnum, in float charidx_frac,
                            out vec3 camera_pos,
                            out vec3 camera_look_at, out vec3 camera_up,
                            out float fovy_deg, out vec3 light_pos)
    {   // Camera and light dispatcher
        if(partnum>=%s) {
    """%partnames[mid]))


for n in partnames[mid:len(partnames)]:
    print(id4("""\
        if(partnum==%s) {
            do_cl_%s(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else
        """%(n, n.lower()), level=2))
#next n

print(id4("""\
        {
            camera_pos=vec3(0.0,0.0,10.0);    //default
            camera_look_at=vec3(0.0);
            camera_up=vec3(0.0, 1.0, 0.0);
            fovy_deg=45.0;
            light_pos=camera_pos;
        }
    } else {
    """))

for n in partnames[0:mid]:
    print(id4("""\
        if(partnum==%s) {
            do_cl_%s(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else
        """%(n, n.lower()), level=2))
#next n

print(dedent("""\
            {
                camera_pos=vec3(0.0,0.0,10.0);    //default
                camera_look_at=vec3(0.0);
                camera_up=vec3(0.0, 1.0, 0.0);
                fovy_deg=45.0;
                light_pos=camera_pos;
            }
        }
    } //do_camera_light
    """))

# vi: set ts=4 sts=4 sw=4 et ai: #

