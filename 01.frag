// Mouseover everything - help is mostly in the tooltips
// Hotkeys work... but only when the editor has focus.
//      Ctrl+S to save - blue outline flash means it worked
//      Alt+Enter to compile - green outline flash means it worked
//      Alt+Up = play/pause
//      Alt+Down = reset to the beginning
// 
// Tabs are real, but autoindent is spaces.  If you want consistency,
// use an outside editor and paste into here rather than the other way around.
//                   ---> http://www.vim.org ;) <---
// ... but if you do use an outside editor, don't forget to save after you 
// paste in an update!

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    float t = iGlobalTime;  // <-- Don't forget to add this!  DON'T FORGET!!!!
                            // You'll see why soon.
    vec2 uv = fragCoord.xy / iResolution.xy;
    fragColor = vec4(uv,0.5+0.5*sin(t),1.0);
}

// vi: set ts=4 sts=4 sw=4 et ai: //

