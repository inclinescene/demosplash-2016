precision highp int; precision highp float;

// Parameters for your demo.

//Geometry
#define QUAD_SIDE_LEN (2.0)
#define TWOSIDED
#define SHADING_RULE (3)
    // 0=>none, 1=>flat, 2=>Lambertian, 3=>Phong

//View
#define ORBIT_RADIUS (6.0)
#define FOVY_DEG (20.0)
    // Sort of like the zoom on a camera --- smaller is closer

// Material
#define SHININESS (64.0)

// Display physics
#define GAMMA (2.2)
#define ONE_OVER_GAMMA (0.45454545454545454545454545454545)

// Routines /////////////////////////////////////////////////////////////

// BASIC 3D ///////////////////////////////////////////

mat4 transpose(in mat4 inMatrix)
{
    // Modified from
    // http://stackoverflow.com/a/18038495/2877364 by
    // http://stackoverflow.com/users/2507370/jeb
    vec4 i0 = inMatrix[0];
    vec4 i1 = inMatrix[1];
    vec4 i2 = inMatrix[2];
    vec4 i3 = inMatrix[3];

    vec4 o0 = vec4(i0.x, i1.x, i2.x, i3.x);
    vec4 o1 = vec4(i0.y, i1.y, i2.y, i3.y);
    vec4 o2 = vec4(i0.z, i1.z, i2.z, i3.z);
    vec4 o3 = vec4(i0.w, i1.w, i2.w, i3.w);

    mat4 outMatrix = mat4(o0, o1, o2, o3);

    return outMatrix;
}

void lookat(in vec3 in_eye, in vec3 in_ctr, in vec3 in_up,
            out mat4 view, out mat4 view_inv)
{
    // From Mesa glu.  Thanks to
    // http://learnopengl.com/#!Getting-started/Camera
    // and https://www.opengl.org/wiki/GluLookAt_code

    vec3 forward, side, up;

    forward=normalize(in_ctr-in_eye);
    up = in_up;
    side = normalize(cross(forward,up));
    up = cross(side,forward);   // already normalized since both inputs are
        //now side, up, and forward are orthonormal

    mat4 orient, where;

    // Note: in Mesa gluLookAt, a C matrix is used, so the indices
    // have to be swapped compared to that code.
    vec4 x4, y4, z4, w4;
    x4 = vec4(side,0);
    y4 = vec4(up,0);
    z4 = vec4(-forward,0);
    w4 = vec4(0,0,0,1);
    orient = transpose(mat4(x4, y4, z4, w4));

    where = mat4(1.0); //identity (1.0 diagonal matrix)
    where[3] = vec4(-in_eye, 1);

    view = (orient * where);

    // Compute the inverse for later
    view_inv = mat4(x4, y4, z4, -where[3]);
    view_inv[3][3] = 1.0;   // since -where[3].w == -1, not what we want
        // Per https://en.wikibooks.org/wiki/GLSL_Programming/Vertex_Transformations ,
        // M_{view->world}
} //lookat

void gluPerspective(in float fovy_deg, in float aspect,
                    in float near, in float far,
                    out mat4 proj, out mat4 proj_inv)
{   // from mesa glu-9.0.0/src/libutil/project.c.
    // Thanks to https://unspecified.wordpress.com/2012/06/21/calculating-the-gluperspective-matrix-and-other-opengl-matrix-maths/

    float fovy_rad = radians(fovy_deg);
    float dz = far-near;
    float sin_fovy = sin(fovy_rad);
    float cot_fovy = cos(fovy_rad) / sin_fovy;

    proj=mat4(0);
    //[col][row]
    proj[0][0] = cot_fovy / aspect;
    proj[1][1] = cot_fovy;

    proj[2][2] = -(far+near)/dz;
    proj[2][3] = -1.0;

    proj[3][2] = -2.0*near*far/dz;

    // Compute the inverse matrix.
    // http://bookofhook.com/mousepick.pdf
    float a = proj[0][0];
    float b = proj[1][1];
    float c = proj[2][2];
    float d = proj[3][2];
    float e = proj[2][3];

    proj_inv = mat4(0);
    proj_inv[0][0] = 1.0/a;
    proj_inv[1][1] = 1.0/b;
    proj_inv[3][2] = 1.0/e;
    proj_inv[2][3] = 1.0/d;
    proj_inv[3][3] = -c/(d*e);
} //gluPerspective

void compute_viewport(in float x, in float y, in float w, in float h,
                        out mat4 viewp, out mat4 viewp_inv)
{
    // See https://en.wikibooks.org/wiki/GLSL_Programming/Vertex_Transformations#Viewport_Transformation
    // Also mesa src/mesa/main/viewport.c:_mesa_get_viewport_xform()

    viewp = mat4(0);
    // Reminder: indexing is [col][row]
    viewp[0][0] = w/2.0;
    viewp[3][0] = x+w/2.0;

    viewp[1][1] = h/2.0;
    viewp[3][1] = y+h/2.0;

    // assumes n=0 and f=1,
    // which are the default for glDepthRange.
    viewp[2][2] = 0.5;  // actually 0.5 * (f-n);
    viewp[3][2] = 0.5;  // actually 0.5 * (n+f);

    viewp[3][3] = 1.0;

    //Invert.  Done by hand.
    viewp_inv = mat4(1.0);
    viewp_inv[0][0] = 2.0/w;    // x->x
    viewp_inv[3][0] = -1.0 - (2.0*x/w);

    viewp_inv[1][1] = 2.0/h;    // y->y
    viewp_inv[3][1] = -1.0 - (2.0*y/h);

    viewp_inv[2][2] = 2.0;      // z->z
    viewp_inv[3][2] = -1.0;

}  //compute_viewport

// RAYCASTING /////////////////////////////////////////

vec4 wts(in mat4 modelviewproj, in mat4 viewport,
                in vec3 pos)
{   // world to screen coordinates
    vec4 clipvertex = modelviewproj * vec4(pos,1.0);
    vec4 ndc = clipvertex/clipvertex.w;
    vec4 transformed = viewport * ndc;
    return transformed;
} //wts

// screen to world: http://bookofhook.com/mousepick.pdf
vec4 WorldRayFromScreenPoint(in vec2 scr_pt,
    in mat4 view_inv,
    in mat4 proj_inv,
    in mat4 viewp_inv)
{   // Returns world coords of a point on a ray passing through
    // the camera position and scr_pt.

    vec4 ndc = viewp_inv * vec4(scr_pt,0.0,1.0);
        // z=0.0 => it's a ray.  0 is an arbitrary choice in the
        // view volume.
        // w=1.0 => we don't need to undo the perspective divide.
        //      So clip coords == NDC

    vec4 view_coords = proj_inv * ndc;
        // At this point, z=0 will have become something in the
        // middle of the projection volume, somewhere between
        // near and far.
    view_coords = view_coords / view_coords.w;
        // Keepin' it real?  Not sure what happens if you skip this.
    //view_coords.w = 0.0;
        // Remove translation components.  Note that we
        // don't use this trick.
    vec4 world_ray_point = view_inv * view_coords;
        // Now scr_pt is on the ray through camera_pos and world_ray_point
    return world_ray_point;
} //WorldRayFromScreenPoint

// HIT-TESTING ////////////////////////////////////////

vec3 HitZZero(vec3 camera_pos, vec3 rayend)
{   // Find where the ray meets the z=0 plane.  The ray is
    // camera_pos + t*(rayend - camera_pos) per Hook.
    float hit_t = -camera_pos.z / (rayend.z - camera_pos.z);
    return (camera_pos + hit_t * (rayend-camera_pos));
} //HitZZero

// --- IsPointInRectXY ---
// All polys will be quads in the X-Y plane, Z=0.
// All quad edges are parallel to the X or Y axis.
// These quads are encoded in a vec4: (.x,.y) is the LL corner and
// (.z,.w) is the UR corner (coords (x,y)).

bool IsPointInRectXY(in vec4 poly_coords, in vec2 world_xy_of_point)
{
    // return true if world_xy_of_point is within the poly defined by
    // poly_coords in the Z=0 plane.
    // I can test in 2D rather than 3D because all the geometry
    // has z=0 and all the quads are planar.

    float x_test, y_test;
    x_test = step(poly_coords.x, world_xy_of_point.x) *
            (1.0 - step(poly_coords.z, world_xy_of_point.x));
        // step() is 1.0 if world.x >= poly_coords.x
        // 1-step() is 1.0 if world.x < poly_coords.z
    y_test = step(poly_coords.y, world_xy_of_point.y) *
            (1.0 - step(poly_coords.w, world_xy_of_point.y));

    return ( (x_test>=0.9) && (y_test >= 0.9) );
        // Not ==1.0 because these are floats!

} //IsPointInRectXY

// CAMERA AND LIGHT ///////////////////////////////////

highp vec3 pos_clelies(in float time, in float radius)
{   //Clelies curve
    //thanks to http://wiki.roblox.com/index.php?title=Parametric_equations
    vec3 pos; float m = 0.8;
    highp float smt = sin(m*time);
    pos.x = radius * smt*cos(time);
    pos.y = radius * smt*sin(time);
    pos.z = radius * cos(m*time);
    return pos;
} //camerapos

void get_cam_and_light(
    in float time,
    out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up,
    out float fovy_deg, out vec3 light_pos)
{
    camera_pos = pos_clelies(time, ORBIT_RADIUS);
    camera_look_at = vec3(0.0);
    camera_up = vec3(0.0, 1.0, 0.0);
    fovy_deg = FOVY_DEG;
    light_pos = camera_pos;
} //get_cam_and_light

// SHADING ////////////////////////////////////////////

float lambertian_shade(in vec3 pixel_pos, in vec3 normal,
                    in vec3 light_pos, in vec3 camera_pos)
{ //Lambertian shading.  Returns the reflectance visible at camera_pos as a
  //result of lighting pixel_pos (having normal) from light_pos.  
  //One-sided object.

    vec3 light_dir = normalize(light_pos - pixel_pos);
    vec3 eye_dir = normalize(camera_pos - pixel_pos);
    if(dot(light_dir, eye_dir) < 0.0) {
        return 0.0;     // Camera behind the object => no reflectance
    } else {
        return max(0.0, dot(light_dir, normal));
            // ^^^^^^^^ light behind the object => no reflectance
    }
} //lambertian_shade

vec3 phong_color(
    in vec3 pixel_pos, in vec3 normal, in vec3 camera_pos,      // Scene
    in vec3 light_pos, in vec3 ambient_color,                   // Lights
    in vec3 diffuse_color, in vec3 specular_color,              // Lights
    in float shininess)                                         // Material
{   // Compute pixel color using Phong shading.  Modified from
    // https://en.wikipedia.org/wiki/Blinn%E2%80%93Phong_shading_model
    // normal must be normalized on input.  All inputs are world coords.
    // Set shininess <=0 to turn off specular highlights.
    // Objects are one-sided.

    vec3 light_dir = normalize(light_pos - pixel_pos);
    vec3 eye_dir = normalize(camera_pos - pixel_pos);

    if(dot(light_dir, eye_dir) < 0.0) {
        return ambient_color;       // Camera behind the object
    }

    float lambertian = max(0.0, dot(light_dir, normal));        // Diffuse

    float specular = 0.0;
    if((lambertian > 0.0) && (shininess > 0.0)) {               // Specular
        vec3 reflectDir = reflect(-light_dir, normal);
        float specAngle = max(dot(reflectDir, eye_dir), 0.0);
        specular = pow(specAngle, shininess);
    }
    /*
    return pow(ambient_color + lambertian*diffuse_color + specular*vec3(1.0),
                vec3(ONE_OVER_GAMMA));
        // TODO Do I need this?
    */
    lambertian = pow(lambertian, ONE_OVER_GAMMA);
    specular = pow(specular, ONE_OVER_GAMMA);

    vec3 retval = ambient_color + lambertian*diffuse_color + 
        specular*specular_color;

    return clamp(retval, 0.0, 1.0);     // no out-of-range values, please!

} //phong_color

// mainImage() //////////////////////////////////////////////////////////

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    float time = iGlobalTime;
    vec2 pixel_coord_01 = fragCoord.xy / iResolution.xy;

    // --- Camera and light ---
    vec3 camera_pos, camera_look_at, camera_up, light_pos;
    float fovy_deg;

    get_cam_and_light(time,
        camera_pos, camera_look_at, camera_up, fovy_deg, light_pos);

    // Camera processing

    mat4 view, view_inv;
    lookat(camera_pos, camera_look_at, camera_up,
            view, view_inv);

    mat4 proj, proj_inv;            // VVVVVVVVV squares are square! :)
    gluPerspective(fovy_deg, iResolution.x/iResolution.y, 1.0, 10.0,
                    proj, proj_inv);

    mat4 viewport, viewport_inv;    // VVVVVVVVV squares are square! :)
    compute_viewport(0.0, 0.0, iResolution.x, iResolution.y,
                        viewport, viewport_inv);

    vec3 material_color = vec3(0.0);    //Color of the quad before shading

    // Raycasting

    vec3 rayend = WorldRayFromScreenPoint(fragCoord,
                                    view_inv, proj_inv, viewport_inv).xyz;
        // rayend-camera_pos is the direction of the ray
    vec3 world_xyz0_of_point = HitZZero(camera_pos, rayend);
        // Where the ray hits z=0
    vec3 normal = vec3(0.0,0.0,-1.0 + 2.0*step(0.0, camera_pos.z));
        // normal Z is -1 if camera_pos.z<0.0, and +1 otherwise.

    // Hit-testing
    float qh = QUAD_SIDE_LEN*0.5;
    vec4 theshape = vec4(-qh,-qh,qh,qh);

    if(IsPointInRectXY(theshape, world_xyz0_of_point.xy)) {

#ifndef TWOSIDED
        material_color = vec3(1.0);
#else
        float front_view = step(0.0, camera_pos.z);
        material_color = vec3(front_view, 0.0, 1.0-front_view);
#endif

    } else {    //we didn't hit
        fragColor = vec4(0.0,0.0,0.0,1.0);  //black
        return; // *** EXIT POINT ***
            // comment out the "return" for a chuckle
    }

    // Shading (it's a shader, after all!)

#if SHADING_RULE == 0
    fragColor = vec4(material_color, 1.0);                  //No shading

#elif SHADING_RULE == 1
    // Flat shading - per-poly
    float reflectance = // VVVVVVV per-poly, lighting as a the poly's center.
        lambertian_shade(vec3(0.0), normal, light_pos, camera_pos);

    float reflectance_gc = pow(reflectance, ONE_OVER_GAMMA);
        // Gamma-correct luminous-intensity reflectance into monitor space.
        // Hey, it's just math, right?  I did this because the quad was too
        // dark otherwise.

    // White light for simplicity
    fragColor = vec4(reflectance_gc * material_color, 1.0);

#elif SHADING_RULE == 2
    //Lambertian shading
    float reflectance = //     VVV Lambertian is per-point, not per-poly
        lambertian_shade(world_xyz0_of_point, normal, light_pos, camera_pos);

    float reflectance_gc = pow(reflectance, ONE_OVER_GAMMA);
    fragColor = vec4(reflectance_gc * material_color, 1.0);

#else
    // Phong shading
    vec3 ambient_color = vec3(0.1);
    vec3 specular_color = vec3(1.0);
    vec3 color = phong_color(
        world_xyz0_of_point, normal, camera_pos, light_pos, 
        ambient_color, material_color, specular_color,  // Light colors
        SHININESS);

    fragColor = vec4(color, 1.0);
#endif
} //mainImage

// vi: set ts=4 sts=4 sw=4 et ai: //

