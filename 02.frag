// ^^^^ check out those shader inputs!

precision highp int;    //play it safe.  Also covers
precision highp float;  //vectors and matrices.

vec4 do_color(in float time, in vec2 coords)
{
    vec4 retval;
    retval.r = coords.x;
    retval.g = coords.y;
    retval.b = 0.5+0.5*sin(time);
    retval.a = 1.0;
    return retval;

    // Exactly the same as the more concise:
    //return vec4(coords,0.5+0.5*sin(time),1.0);

    // Note: we just found out where the coordinate origin is!
    // Debug with colors rather than printf().

} //do_color

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    float t = iGlobalTime;
    vec2 uv = fragCoord.xy / iResolution.xy;
    fragColor = do_color(t, uv);
    // Why should you use functions?  How about this:
    //fragColor = do_color(t*15.0, uv);
}

// vi: set ts=4 sts=4 sw=4 et ai: //

