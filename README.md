# Inside a Demotool (new!)

by cxw/Incline - to be presented at Demosplash 2017

[InsideADemotool.pptx](InsideADemotool.pptx).

Refers to [lua-osg-livecoding](https://bitbucket.org/inclinescene/lua-osg-livecoding).

Rather than create a new repo for one file, I piggybacked on this one.
The Demosplash 2016 material is...

# Introductory Democoding with [ShaderToy](https://www.shadertoy.com)

by cxw/Incline - presented at Demosplash 2016

 - See the [PDF of the slides](IntroductoryDemocodingwithShaderToy.pdf) - 
    click the link, then click "View Raw".
 - Or try the textual [OUTLINE.md](OUTLINE.md) for 
    specifics about the presentation.

Any updates will be at
<https://bitbucket.org/inclinescene/demosplash-2016>.

## Organization

 - Shaders 01.frag through 09.frag are the shaders I demonstrated live.
   #08 has two parts: Image ([08-gfx.frag](08-gfx.frag)) and Sound
   ([08-msx.frag](08-msx.frag)).
 - The [PDF](IntroductoryDemocodingwithShaderToy.pdf) and 
   [PPTX](IntroductoryDemocodingwithShaderToy.pptx) are the slides.  
   The SVGs are masters of the dataflow diagrams in the slides.
 - [gen-music.py](gen-music.py) is the Python script 
   I mentioned that generates the GLSL
   encoding of the song in 08-msx.frag.  
 - [update-message](update-message) is a bash script
   that stuffs the output of gen-music.py into 08-msx.frag so I don't have
   to copy and paste it by hand.
 - multipart.frag, music.frag, and make-message.py are from
   [ncl00](https://www.shadertoy.com/view/lsySDD) and are provided for your 
   amusement, I mean, edification.
 - .gitignore is a file you can ignore ;) if you are not cloning this repo
   to do further development.

## Useful links

### WebGL

 - <https://www.khronos.org/files/webgl/webgl-reference-card-1_0.pdf>
     - You want this!  The last two pages ("OpenGL ES Shading Language 1.0")
        are the part that's relevant to users of ShaderToy.  The first two
        pages are in case you want to hack on the ShaderToy site itself!
 - <https://dev.opera.com/tags/webgl/>
 - <http://stackoverflow.com/q/5855223/2877364>

### Matrices

 - <http://www.purplemath.com/modules/mtrxinvr.htm>
 - <https://www.opengl.org/wiki/Compute_eye_space_from_window_space>
    (although I didn't wind up using this much)
 - <https://en.wikibooks.org/wiki/GLSL_Programming/Vertex_Transformations>

### Raycasting

 - <http://antongerdelan.net/opengl/raycasting.html>
 - <http://bookofhook.com/mousepick.pdf>
    (linked to by <https://gist.github.com/gyng/8921328>)
 - <http://lodev.org/cgtutor/raycasting.html>
 - <https://www.opengl.org/wiki/Compute_eye_space_from_window_space>

### Physics of light

 - <http://www.poynton.com/notes/colour_and_gamma/GammaFAQ.html#gamma_correction>

## License

 - All textfiles and slides CC-BY-NC 4.0 International.
 - Shaders 01-08 are CC-BY-NC 4.0 International.
 - Shader 09 is CC-BY-SA 3.0.  If you pull out the SA routines,
   the rest is also licensed CC-BY-NC 4.0 International.
 - Attribution required for the above: "From or based on code by cxw/Incline /
    <http://www.devwrench.com> / <https://www.shadertoy.com/user/cxw> /
    <https://demozoo.org/sceners/65996/>"
 - Reminder: NO WARRANTY!!!

// vi: set ts=2 sts=2 sw=2 et ai syntax=markdown: //

