# Presentation notes for demosplash2016-cxw

cxw/Incline

Any updates will be at
<https://bitbucket.org/inclinescene/demosplash-2016>.  
Also see [README.md](README.md) for more.

## Outline

  - Brief intro to ShaderToy - *show "Pirates"*
    <https://www.shadertoy.com/view/ldXXDj>
  - Can you do multiple scenes?  Yes! - *show ncl00* (except that I wasn't
    able to get it to run at the party)
      <https://www.shadertoy.com/view/lsySDD>
  - Architectural diagram of a ShaderToy demo
    - What are shaders?
    - What does the code you write actually do?
  - Live walkthrough
    1. default shader.  Overview of GLSL and the ShaderToy environment.
      <https://www.shadertoy.com/view/4lVGWm>
    2. functions and some other useful things.
      <https://www.shadertoy.com/view/4tK3Wm>
    3. builtins, and compositing effects.  *oldschool effect: basic tunnel*
      <https://www.shadertoy.com/view/XtV3Wm>
    4. Drop the sine tunnel onto the screen with bounce, oldschool style
      <https://www.shadertoy.com/view/Mly3Ry>
    5. Two sines with different centers => moire
      <https://www.shadertoy.com/view/lly3zK>
    6. Loops and arrays.
        *oldschool effect: nonuniformly spaced vertical blinds*
          <https://www.shadertoy.com/view/MlVGRK>
    7. Multiple scenes in a demo.  Blinds in and out, bounce in, fade out.
      <https://www.shadertoy.com/view/lty3R3>
    8. Sound: basic chiptuning
      <https://www.shadertoy.com/view/MtKGR3>
  - 3D stuff
    9.  (09) Raycasting: rotating quad by raycasting the screen to the plane and
        checking for intersections.
          <https://www.shadertoy.com/view/MtyGDV>
        - Bonus: includes lighting with a raycast quad!

## Bonus topics

There wasn't time at the party to cover these, but let me know if you're
interested in hearing about them.

  - Texturing with built-in textures
  - Texturing with dynamic content.  *Effect:*
    Rotating quad, raycast, with sine moire on one side of the quad and
    a Venetian-blind tunnel on the other side of the quad.
  - 3D: rotating quad by projecting corners to the screen and filling them.

## Some other cool shaders (not by cxw)

 - <https://www.shadertoy.com/view/MdGGzR> - the one I showed with the
    multiple buffers and the mouse response
 - <https://www.shadertoy.com/view/Xs3XW4> - the C64 BASIC interpreter in a
    shader!
 - <https://www.shadertoy.com/view/ldcSDB> - another mouse, multibuffer one
 - <https://www.shadertoy.com/view/Ms2XWw> - a twist scroller I didn't show

## A debugging story

I had a `get_blind_mask` problem in #07:
four black lines between the middle blinds
in a Chrome window that was 1198x862 (image size 500x281).  I had
`get_blind_mask` return internal values rather than 0/1.  I then had
`do_colors` return `vec4(vec3(blind_mask),1.0)`. I then set the time `t`
in `mainImage` to a static value that exhibited the problem (another
win for `t`!).  I was then able to compile, get a static picture of the
internal variables, right-click on the canvas, copy image, paste the image
into GIMP as a layer, and see what was going on.  It turned out some
x values were not >= one edge or <= the next edge, once all the computation
was done.  So I added a hack: if the whole image should be displayed,
just display it and don't try to compute everything!

## Links

 - Damped sinusoid: <https://ocw.mit.edu/courses/mathematics/18-03sc-differential-equations-fall-2011/unit-ii-second-order-constant-coefficient-linear-equations/damped-harmonic-oscillators/MIT18_03SCF11_s13_2text.pdf>
 - DuckTales theme: tweaked from
<https://e17dd140-a-62cb3a1a-s-sites.googlegroups.com/site/manolitomystiq/DuckTales-MoonTheme.pdf?attachauth=ANoY7cpzZUN5gTdR1hohCEP5ptAGjJXlh3S2XLOm_A8HkQYwIsD7q7YpgY0g40sYMIKggxKV_GgEW4L-UgR0vl8ze7i8hS_YDSPO8pKg7oPtQFgpecvgFCaW45s4k0gPo8o6gpfpNcN_p5D7EzW9NvubzPMe8x9WcNMyBcwvYw_3yRR1Z-W8ab8sU9SDqLz1wHfWx4l4g5j7GB23KprdS3ApadLAUypcYW7WDsrrkGfq1LXatLBsNqc%3D&attredirects=0>

This file CC-BY-NC 4.0 International (see [README.md](README.md) for license details)

// vi: set ts=2 sts=2 sw=2 et ai syntax=markdown: //

