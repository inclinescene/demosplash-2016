//08-gfx.frag: Visuals
precision highp int;precision highp float;

// Parameters for your demo.  These should match 08-msx!!

// Song parameters
#define BPS (1.8333333333333333333333333333333)//(1.5)
    // beats per sec = BPM/60 (110 bpm) (90 bpm)
#define SPB (0.54545454545454545454545454545455)//(0.66666666666666666666666666666667)
    // sec per beat, precomputed
#define BPP (128.0)
    // beats per pattern

#define FLASH_DURATION_BPCT (0.25)
    // How long the flash is, in percentage of a beat
#define FLASH_SKEW_BPCT (0.25)
    // Where in the beat the flash happens.  Tweak to sync.
    // NOTE: Use Firefox rather than Chrome --- sync seems iffy on Chrome.

#define MUSIC_START_TIME (1.0)
    // how long to wait before starting the music

// Computed parameters //////////////////////////////////////////////////
#define PATTERN_DURATION_SEC (SPB * BPP)
    // = SPP = S/B * B/P

// Routines /////////////////////////////////////////////////////////////

vec3 hsv2rgb(vec3 c) {
    // All inputs range from 0 to 1.  By hughsk, from
    // https://github.com/hughsk/glsl-hsv2rgb/blob/master/index.glsl .
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
} //hsv2rgb

// mainImage() //////////////////////////////////////////////////////////

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    vec2 pixel_coord_01 = fragCoord.xy / iResolution.xy;

    float music_t = iGlobalTime-MUSIC_START_TIME;
    float sat;
    if(music_t < 0.0) {     // Fade in from white while waiting
        sat = smoothstep(-MUSIC_START_TIME, 0.0, music_t);

    } else {                // Sync to the music
        music_t = mod(music_t, PATTERN_DURATION_SEC); //within one pattern

        float beat_in_pattern = mod(music_t*BPS, BPP);
            // including fraction
        float time_in_beat = mod(music_t, SPB);

        float is_even_beat = step(1.0, mod(beat_in_pattern, 2.0));
            // Flash on beats 2 and 4 of each measure.

        float sat_in_beat = (1.0 - step(FLASH_SKEW_BPCT, time_in_beat));
            // full saturation before FLASH_SKEW_BPCT
        sat_in_beat +=
            smoothstep(FLASH_SKEW_BPCT, FLASH_DURATION_BPCT + FLASH_SKEW_BPCT,
                        time_in_beat);
            // Sat drops to 0 at FLASH_SKEW_BPCT, then recovers over
            // FLASH_DURATION_BPCT.

        sat = mix(1.0, sat_in_beat, is_even_beat);
            // Full sat (1.0) if it's not an even beat; computed sat if it is.
            //TODO full sat for the first second
    } //endif music_t<0.0 else

    fragColor = vec4(     // A nice color stripe pattern
        hsv2rgb(vec3(pixel_coord_01.x+pixel_coord_01.y+music_t, sat, 1.0)),
        1.0);                                        //         ^^^
} //mainImage

// vi: set ts=4 sts=4 sw=4 et ai: //

